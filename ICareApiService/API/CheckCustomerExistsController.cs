﻿using ICareApiService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ICareApiService.API
{
    public class CheckCustomerExistsController : ApiController
    {
        CheckCustomerExistsDataContext _Context = new CheckCustomerExistsDataContext();
        // GET: CheckCustomerExists

        //[Authorize]
        public SP_CheckCustomerExistsResult Get(string firstName, string lastName, string phoneNumber, DateTime dateOfBirth, string companyId)
        {
            var result = _Context.SP_CheckCustomerExists(firstName, lastName, phoneNumber, dateOfBirth, companyId);
            return result.FirstOrDefault();
        }

        //public void Get(string firstName)
        //{
        //    var result = _Context.Patients.Where(x => x.FirstName == firstName);
        //}
    }
}