﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetInsCarrierWithPlanByStoreController : ApiController
    {
        
        // GET: GetInsCarrierWithPlanByStore

        //[Authorize]
        public List<SP_GetInsCarrierWithPlanByStoreResult> Get(string id)
        {
            GetInsCarrierWithPlanByStoreDataContext _Context = new GetInsCarrierWithPlanByStoreDataContext();
            var result = _Context.SP_GetInsCarrierWithPlanByStore(id);
            return result.ToList();
        }
    }
}