﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetReasonForVisitDetailsController : ApiController
    {
        GetReasonForVisitDetailsDataContext _Context = new GetReasonForVisitDetailsDataContext();

        //[Authorize]
        public List<SP_GetReasonForVisitDetailsResult> Get(int id)
        {
            var result = _Context.SP_GetReasonForVisitDetails(id);
            return result.ToList();
        }
    }
}
