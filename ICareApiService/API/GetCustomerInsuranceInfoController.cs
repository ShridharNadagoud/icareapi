﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ICareApiService.API
{
    public class GetCustomerInsuranceInfoController : ApiController
    {
        GetCustomerInsuranceInfoDataContext _Context = new GetCustomerInsuranceInfoDataContext();
        // GET: GetCustomerInsuranceInfo
        [System.Web.Http.Authorize]
        public IList<SP_CustomerInsuranceInfoResult> Get(string companyId, int? patientId)
        {
            var result = _Context.SP_CustomerInsuranceInfo(companyId, patientId);
            return result.ToList();
        }
    }
}