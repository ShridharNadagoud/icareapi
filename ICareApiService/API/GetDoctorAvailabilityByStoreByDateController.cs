﻿using ICareApiService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{

    public class GetDoctorAvailabilityByStoreByDateController : ApiController
    {
        GetDoctorAvailabilityByStoreByDateDataContext _Context = new GetDoctorAvailabilityByStoreByDateDataContext();
        
       // [Authorize]
        public List<SP_GetDoctorAvailabiltyByStoreByDateResult> Get(int storeId, DateTime appointmentDate)
        {
            var result = _Context.SP_GetDoctorAvailabiltyByStoreByDate(storeId, appointmentDate);
            return result.ToList();
        }
    }
}
