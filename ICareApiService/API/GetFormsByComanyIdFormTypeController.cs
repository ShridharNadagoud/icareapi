﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetFormsByComanyIdFormTypeController : ApiController
    {
        public HttpResponseMessage GetFormsByComanyIdFormType(string companyId,int formId)
        {
            if (String.IsNullOrEmpty(companyId)||formId==0)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            string fileName;
            switch (formId)
            {
                case 1:
                    fileName = "Patient Form 1.pdf";
                    break;
                case 2:
                    fileName = "Patient Form 2.pdf";
                    break;
                case 3:
                    fileName = "Patient Form 3.pdf";
                    break;
                case 4:
                    fileName = "Patient Form 4.pdf";
                    break;
                default:
                    fileName = "Patient Form 5.pdf";
                    break;
            }
            string localFilePath= System.Web.HttpContext.Current.Request.MapPath("~\\Documents\\"+companyId+"\\"+formId+".pdf");
            int fileSize=int.MaxValue;

            //localFilePath = getFileFromID(id, out fileName, out fileSize);
            try
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = fileName;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                return response;
            }
            catch
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.NotFound);
                return response;
            }
            
        }
    }
}
