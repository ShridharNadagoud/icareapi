﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetLocationDetailController : ApiController
    {
        GetLocationDetailDataContext _Context = new GetLocationDetailDataContext();

        // GET: GetLocationDetail
        //[Authorize]
        public List<SP_GetLocationDetailResult> Get(string text, string companyId)
        {
            var result = _Context.SP_GetLocationDetail(companyId, text);
            return result.ToList();
        }
    }
}