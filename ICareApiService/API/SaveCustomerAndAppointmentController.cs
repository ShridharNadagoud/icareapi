﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ICareApiService.API
{
    public class SaveCustomerAndAppointmentController : ApiController
    {
        private int? appointmentid = 0;
        SaveCustomerAndAppointmentDataContext _Context = new SaveCustomerAndAppointmentDataContext();

        //[Authorize]
        //public string Get(int? patientId, int doctorId, int locationId, string companyId, string firstName, string lastName, string phoneNumber, int title, string sex, string email, string officeNum, string birthDate, string address1, string address2, string city, string state, string zipCode, string addressTypeId, int planId, string insuredFirstName, string insuredLastName, string insuredPhoneNumber, string insuredId
        //    , string insuredSex, string insuredDob, string insuredAddress1, string insuredAddress2, string insuredCity, string insuredState, string insuredZip,
        //    int appointmentTypeId,string appointmentDate, string appointmentStartTime, string appointmentEndTime, string appointmentNotes, string recordedByComputer)
        //{
        //  var id=  Post(patientId, doctorId, locationId, companyId, firstName, lastName, phoneNumber, title, sex, email, officeNum, Convert.ToDateTime(birthDate), address1, address2, city, state, zipCode, addressTypeId, planId, insuredFirstName, insuredLastName, insuredPhoneNumber, insuredId, insuredSex,Convert.ToDateTime(insuredDob),insuredAddress1,insuredAddress2,insuredCity,insuredState,insuredZip
        //    ,appointmentTypeId, Convert.ToDateTime(appointmentDate), appointmentStartTime, appointmentEndTime, appointmentNotes, recordedByComputer);
        //    return id;
        //}

       // [Authorize]
        [ActionName("Post")]
        public string Post(int? patientId, int doctorId, int locationId, string companyId, string firstName,string lastName, string phoneNumber, int? title,string sex,string email,int homeOffice,DateTime birthDate,string address1, string address2,string city,string state,string zipCode,string addressTypeId,int planId,string insuredFirstName,string insuredLastName,string insuredPhoneNumber,string insuredId, string insuredSex, DateTime insuredDob, string insuredAddress1, string insuredAddress2,string insuredCity, string insuredState,string insuredZip, int appointmentTypeId, DateTime appointmentDate, string appointmentStartTime, string appointmentEndTime, string appointmentNotes, string recordedByComputer)
        {
            //TBD
            addressTypeId = null;
            patientId=patientId == 0 ? null : patientId;
            //TBD Appointment
            bool appointmentCancelled = false;
            bool appointmentConfirmed = true;
            bool isAppLeftMessage = false;
            bool isNotAvailable = false;
            bool isPreAppointment = false;
            int recurringAppointmentId = 0;
            int appIcon1 = 0;
            int appIcon2 = 0;
            int appIcon3 = 0;
            int appIcon4 = 0;
            bool isRecurrenceVariance = false;
            bool isRecheck = false;
            int medicalPatientInsuranceID = 0;
            string eMRAppointmentNum = null;
            int appointmentSource = 0;
            int userNo = 50708; //MrBoss EmpId


            //CompanyID,FirstName,LastName,PhoneNumber,Title,Gender,emailid,StoreId,DateOfBirth
                                                          //patientId,companyId,firstName,lastName,title,phoneNumber,sex,email, homeOffice, birthDate,address1,address2,city,state,zipCode,addressTypeId,isPrimary,planId,insuredRelationshipId, insuredFirstName, insuredLastName, insuredSex, insuredPhoneNumber, insuredId, insuredBirthdate, insuredAddress1, insuredAddress2, insuredCity, insuredState, insuredZip
           var value=_Context.SP_SaveCustomerAndAppointment(patientId,companyId,firstName,lastName,title,phoneNumber,sex,email, homeOffice, birthDate,address1,address2,city,state,zipCode,addressTypeId,true,planId,301,insuredFirstName,insuredLastName, insuredSex, insuredPhoneNumber,insuredId,insuredDob,insuredAddress1,insuredAddress2,insuredCity,insuredState,insuredZip);

            // var customerId = (from p in value select 1* p.CusttomerId).FirstOrDefault();
            //null, locationId, doctorId, appointmentTypeId, null, Convert.ToInt32(value), appointmentDate, appointmentStartTime, appointmentEndTime, appointmentNotes, phoneNumber, appointmentCancelled, appointmentConfirmed, isAppLeftMessage, isNotAvailable, "", 0, isPreAppointment, recurringAppointmentId, appIcon1, appIcon2, appIcon3, appIcon4, recordedByComputer,userNo , isRecurrenceVariance, isRecheck, medicalPatientInsuranceID, "123456789", eMRAppointmentNum, appointmentSource, ref appointmentid
            var response = value.FirstOrDefault();
            int? appt_no = null;int locationID = locationId;
            int resourceID = doctorId;
            int serv_no = appointmentTypeId;
            int? ins_no = response.CustomerInsuranceId;
            int patientID = response.CustomerId;
           DateTime appt_date = appointmentDate;
            var appt_start_time = appointmentStartTime;
            var appt_end_time = appointmentEndTime;
            var appt_notes = appointmentNotes;
            var appt_phone = phoneNumber;
            var appt_cancel_ind = appointmentCancelled;
            var appt_confirmed_ind = appointmentConfirmed;
            var appt_left_msg_ind = isAppLeftMessage;
            var appt_no_answer_ind=isNotAvailable;
            var appt_conf_inits = "";
            var appt_show_ind = 0;
            var appt_pre_appointment = isPreAppointment;
            var recurringAppointmentID = recurringAppointmentId;
            var appt_icon_1 = appIcon1;
            var appt_icon_2 = appIcon2;
            var appt_icon_3=appIcon3;
            var appt_icon_4 = appIcon4;
            var computerName = recordedByComputer;
            //var userNo= userNo;
            //var isRecurrenceVariance = isRecurrenceVariance;
            //var isRecheck = isRecheck;
            var medicalInsurance = medicalPatientInsuranceID;
            var appointmentUid = "123456789";
           var  emrAppointmentNum = eMRAppointmentNum;
            // var appointmentSource = appointmentSource; 
            //var  appointmentId= ref appointmentid;


            int? appointmentId = null;
            var result  = _Context.SP_Save_AppointmentDetail(appt_no, locationID, resourceID, serv_no, ins_no, patientID, appt_date, appt_start_time, appt_end_time, appt_notes, appt_phone, appt_cancel_ind, appt_confirmed_ind, appt_left_msg_ind, appt_no_answer_ind, appt_conf_inits, appt_show_ind, appt_pre_appointment, recurringAppointmentID, appt_icon_1, appt_icon_2, appt_icon_3, appt_icon_4, computerName, userNo, isRecurrenceVariance, isRecheck, medicalInsurance, appointmentUid, emrAppointmentNum, appointmentSource,ref appointmentId);
            return "true";
        }

    }
}
 