﻿using ICareApiService.LinqToSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetStoreDetailsByCompanyIDController : ApiController
    {
        
        GetStoreDetailsByCompanyIDDataContext _Context = new GetStoreDetailsByCompanyIDDataContext();

        // GET: GetStoreDetailsByCompanyID
        //[Authorize]
        public List<GetStoreDetailsByCompanyIDResult> Get(string companyId)
        {
            var result = _Context.GetStoreDetailsByCompanyID(companyId);
            return result.ToList();
        }
    }
}
