﻿using ICareApiService.LinqToSql.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile
{
    public class GetCustomerDetailByCustomerIdController : ApiController
    {
        GetCustomerDetailByCustomerIdDataContext _Context = new GetCustomerDetailByCustomerIdDataContext();

        //[Authorize]
        public List<GetCustomerDetailByCustomerIdResult> Get(string customerId)
        {
            int cusID = Int32.Parse(customerId);
            var customerDetailList = _Context.GetCustomerDetailByCustomerId(cusID);
            return customerDetailList.ToList();
        }
    }
}
