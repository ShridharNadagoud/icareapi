﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class DeleteAppointmentScheduleController : ApiController
    {
        DeleteAppointmentScheduleDataContext _dataContext = new DeleteAppointmentScheduleDataContext();
        public int Get(int customerId, DateTime dateOfAppointment, string startTime)
        {
            return Post(customerId, dateOfAppointment, startTime);
        }

        public int Post(int customerId, DateTime dateOfAppointment,string startTime)
        {
            try
            {
                var result = _dataContext.DeleteAppointmentSchedule(customerId, dateOfAppointment, startTime);
                return result; //returning 0 that is updated successfully
            }
            catch(Exception )
            {
                return 1; //Not updated successfully
            }
        }
    }
}
