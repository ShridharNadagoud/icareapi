﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class UpdateDoctorInfoController : ApiController
    {

        public int Get(string UserName, string Password, string FirstName, string LastName, string PhoneNumber, int CompanyID, string Specialization,string EmployeeId)
        {
            var id = Post(UserName, Password, FirstName, LastName, PhoneNumber, CompanyID,Specialization, EmployeeId);
            return id;
        }

        // POST: api/UpdateDoctorInfo
        [ActionName("Post")]
        public int Post(string UserName, string Password, string FirstName, string LastName, string PhoneNumber, int CompanyID, string Specialization, string EmployeeId)
        {
            int? ClientKey = null;
            try
            {
                UpdateDoctorInfoDataContext _context = new UpdateDoctorInfoDataContext();
                var result = _context.UpdateDoctorInfo(UserName, Password, FirstName, LastName, PhoneNumber,  CompanyID, Specialization, EmployeeId);
                return result;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
    }
}
