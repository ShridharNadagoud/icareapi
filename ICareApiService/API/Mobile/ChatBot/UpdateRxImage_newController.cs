﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class UpdateRxImage_newController : ApiController
    {
        int CustomerDocumentId;
        int i = 0;
        UpdateRxImage_newDataContext context = new UpdateRxImage_newDataContext();
        public int Get(int img2, int img3, int img4, int img5, int img6, int img7, int img8, int img9, int img10, int customerExamId, int CustomerId)
        {
            return Post(img2, img3, img4, img5, img6, img7, img8, img9, img10, customerExamId, CustomerId);
        }
        public int Post(int img2, int img3, int img4, int img5, int img6, int img7, int img8, int img9, int img10, int customerExamId, int CustomerId)
        {
            try
            {
                int[] arr = new int[10];
                arr[0] = img2;
                arr[1] = img3;
                arr[2] = img4;
                arr[3] = img5;
                arr[4] = img6;
                arr[5] = img7;
                arr[6] = img8;
                arr[7] = img9;
                for (i = 0; i < 10; i++)
                {
                    if (arr[i]!=0)
                    {
                        CustomerDocumentId = arr[i];
                        var result = context.UpdateRxImage_new(customerExamId, CustomerId, CustomerDocumentId);
                    }
                   
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
    }
}
