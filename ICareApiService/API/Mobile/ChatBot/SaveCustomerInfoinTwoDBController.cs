﻿using ICareApiService.LinqToSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SaveCustomerInfoinTwoDBController : ApiController
    {
        //Guid guidvalue = new Guid();
        //Guid customerGuid = new Guid();
        string customerGuid;
        int companyId;
        public string Get(string firstName, string lastName, string phoneNumber, DateTime dateOfBirth, string emailId, string Gender, int age, int doctorId, string address1, string address2, string city, string state, string zipCode, string country)
        {
            var id = Post(firstName, lastName, phoneNumber, dateOfBirth, emailId, Gender, age, doctorId, address1, address2, city, state, zipCode, country);
            return id;
        }
        public string Post(string firstName, string lastName, string phoneNumber, DateTime dateOfBirth, string emailId, string Gender, int age, int doctorId, string address1, string address2, string city, string state, string zipCode, string country)
        {
            string password = string.Empty;
            SaveCustomerInformationResult customer = new SaveCustomerInformationResult();
            SaveCustomerInfoResult customerData = new SaveCustomerInfoResult();
            try
            {
                SaveCustomerInfoDataContext _contect = new SaveCustomerInfoDataContext();
                var customers = _contect.SaveCustomerInfo(firstName, lastName, phoneNumber, dateOfBirth, emailId, Gender, age, doctorId, address1, address2, city, state, password, zipCode, country);
                customerData = customers.FirstOrDefault();
                customerGuid = customerData.CustomerUid.ToString();
                return customerData.ID.ToString() + "|" + customerGuid;
                //if (customerData != null)
                //{                    
                //    customerGuid = customerData.CustomerUid.ToString();//new Guid(customerData.CustomerUid.ToString()); 
                //    companyId = Int32.Parse(customerData.CompanyID);
                //    try
                //    {
                //        SaveCustomerInfoOpDataContext _contextOpti = new SaveCustomerInfoOpDataContext();
                //        var Customers = _contextOpti.SaveCustomerInformation(companyId, firstName, lastName, phoneNumber, dateOfBirth, emailId, Gender, age, doctorId, address1, address2, city, state, zipCode, country, password, customerGuid);
                //        _contextOpti.SubmitChanges();
                //        customer = Customers.FirstOrDefault();
                //    }
                //    catch (Exception ex)
                //    {
                //        return "OptiCommerce: " + ex.Message;
                //    }
                //    return customerData.ID.ToString()+"|"+ customerGuid;
                //}
                //else
                //{
                //    return "-1";
                //}
            }
            catch (Exception ex)
            {
                return "OptiBot1: " + ex.Message;
            }
        }


    }
}
