﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetCustomerDetailByIdController : ApiController
    {
        GetCustomerDetailByIdDataContext _Context = new GetCustomerDetailByIdDataContext();

        [Authorize]
        public List<GetCustomerDetailByIdResult> Get(string doctorID, string customerId)
        {
            int doctID = Int32.Parse(doctorID);
            int cusID = Int32.Parse(customerId);
            var customerDetailList = _Context.GetCustomerDetailById(cusID, doctID);
            return customerDetailList.ToList();
        }
    }
}
