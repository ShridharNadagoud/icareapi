﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetAppointmentDetailsByDateController : ApiController
    {
        string message;
        DateTime ParameterDate;
        int count=0;
        sp_GetAppointmentDetailsByDateDataContext _context = new sp_GetAppointmentDetailsByDateDataContext();
        public void Get()
        {
            string TodayDate = DateTime.Now.ToString();
            DateTime oDate = Convert.ToDateTime(TodayDate).Date;
            DateTime SixMnthsBackDate = oDate.AddMonths(-6).Date;
            for (count = 0; count < 2; count++)
            {
                if (count == 0)
                {
                    ParameterDate = oDate;
                }
                else
                {
                    ParameterDate = SixMnthsBackDate;
                }
                var appointmentList = _context.sp_GetAppointmentDetailsByDate(ParameterDate);
                var test = appointmentList.Select(x => x).ToList();
                var customerIdList = test.Select(x => x.CustomerId).ToList();
                if (customerIdList != null && customerIdList.Count > 0)
                {
                    foreach (var customerId in customerIdList)
                    {
                        var list = test.Where(x => x.CustomerId == customerId).ToList();
                        string customerFullName = list.FirstOrDefault().CustomerFistName + " " + list.FirstOrDefault().CustomerLastName;
                        using (System.Net.WebClient client = new System.Net.WebClient())
                        {

                            message = HttpUtility.UrlEncode("Dear" + " " + customerFullName + ", This is a reminder for your appointment:" + list.FirstOrDefault().AppointmentStartTime + "  with Dr. :" + list.FirstOrDefault().DoctorFirstName);
                            using (var wb = new WebClient())
                            {
                                byte[] response = wb.UploadValues("https://api.textlocal.in/send/", new NameValueCollection()
                {
                {"apikey" , "lsCva7URzlY-DqEiSrm5feDEMbYBTPcJoW6WePS3g7"},
                {"numbers" , "919177414341"},
                {"message" , message},
                {"sender" , "TXTLCL"}
                });
                                string result = System.Text.Encoding.UTF8.GetString(response);
                                result.Contains("success");
                            }
                        }
                    }
                }
            }
        }
    }
}
