﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SlotAvailabilityController : ApiController
    {
        SlotAvailabilityDataContext _context = new SlotAvailabilityDataContext();
        public List<Sp_SlotAvailabilityResult> Get(int doctorId, DateTime dateOfAppointment)
        {
            var availableSlotTime = _context.Sp_SlotAvailability(doctorId, dateOfAppointment);
            return availableSlotTime.ToList();
        }
    }
}
