﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web.Http;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using ICareApiService.LinqToSql.Mobile.ChatBot;
using Google.Apis.Util.Store;
using System.Threading;
using System.Web;
using File = Google.Apis.Drive.v2.Data.File;
using IFile = Google.Apis.Drive.v3.Data.File;
using System.Web.Http;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
namespace ICareApiService.API.Mobile.ChatBot
{
    public class SaveImagesIntoAzureOptiBuddyController : ApiController
    {
        const string storageAccountName = Util.Patientdocument;
        const string StorageAccountKey =  Util.StorageAccountKey;

        private static Random random = new Random();
        string fileId;
        string singlefilePathName = string.Empty;
        string AzurePath;
        string CurrentDate;
        static string[] scopes = { DriveService.Scope.Drive };
        public string Post(int companyId,int customerId)
        {
            try
            {
                CurrentDate = DateTime.Now.ToString(Util.DateFormate);
                var httpRequest = HttpContext.Current.Request;
                string fileName = string.Empty, fileContentType = string.Empty;
                        var postedFile = httpRequest.Files[0];
                        fileName = postedFile.FileName;
                        if (fileName.Contains("\\") || fileName.Contains(@"\"))
                        {
                            fileName = postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault();
                        }
                        var data = fileName.Split('.');
                        fileContentType = data.LastOrDefault();
                        Stream str = postedFile.InputStream;
                        BinaryReader Br = new BinaryReader(str);
                        
                        AzurePath = Path.Combine(HttpContext.Current.Server.MapPath(Util.GDrive + postedFile.FileName));
               
                        string resized = ResizeImage(str, new Size(4752 ,3168), AzurePath);
                        try
                        {
                            Microsoft.WindowsAzure.Storage.Auth.StorageCredentials creden = new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, StorageAccountKey);

                            Microsoft.WindowsAzure.Storage.CloudStorageAccount acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(creden, useHttps: true);

                            CloudBlobClient client = acc.CreateCloudBlobClient();

                            CloudBlobContainer cont = client.GetContainerReference(Util.Patientdocument);

                            cont.CreateIfNotExists();

                            cont.SetPermissions(new BlobContainerPermissions
                            {

                                PublicAccess = BlobContainerPublicAccessType.Blob
                            });
                            var optiBuddy =Util.OptiBuddy;
                            var CustomerExam = Util.CustomerExam;
                    //Azure Path
                            CloudBlockBlob cblob = cont.GetBlockBlobReference(optiBuddy +"/"+ companyId+"/"+customerId+"/"+ CustomerExam+"/" + CurrentDate + "/" + postedFile.FileName);
                            using (var imagestream = new System.IO.FileStream(AzurePath, System.IO.FileMode.Open))
                            {
                                cblob.UploadFromStream(imagestream);
                            }
                            var filename = cblob.Uri;
                            fileId = filename.ToString();
                        }
                        catch (Exception ex)
                        {
                            return ex.ToString();
                        }
                        //Delete Temp Images
                        var deletePath = HttpContext.Current.Server.MapPath(Util.GDrive);
                        string[] filePaths = Directory.GetFiles(deletePath);
                        foreach (string filePath in filePaths)
                            System.IO.File.Delete(filePath);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return fileId;
        }

        //Compress Image

        private string ResizeImage(Stream localimage, Size size, string targetPath)
        {

            int originalWidth = size.Width;
            int originalHeight = size.Height;
            float percentWidth = (float)size.Width / (float)originalWidth;
            float percentHeight = (float)size.Height / (float)originalHeight;
            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;

            // Image newImage = new Bitmap(newWidth, newHeight);
            using (var image = System.Drawing.Image.FromStream(localimage))
            {
                var newWidth = (int)(image.Width * 0.7);
                var newHeight = (int)(image.Height * 0.7);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.PixelOffsetMode = PixelOffsetMode.HighQuality;
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
            return targetPath;
        }

    }
}