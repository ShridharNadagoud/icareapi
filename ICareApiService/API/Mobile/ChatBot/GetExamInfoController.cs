﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetExamInfoController : ApiController
    {
        GetExamInfoDataContext _Context = new GetExamInfoDataContext();

        [Authorize]
        public List<GetExamInfoResult> Get(int id)
        {
            var result = _Context.GetExamInfo(id);
            return result.ToList();
        }
    }
}
