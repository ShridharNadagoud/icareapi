﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class CreateAppointmentScheduleController : ApiController
    {
        [Authorize]
        public void Get(int customerId, int nextVisitedDoctorid, DateTime dateOfAppointment, string startTime, string endTime, bool isConfirmed, int reasonForVisitId, string appointmentNotes,int StoreId, int doctorScheduleId)
        {
            Post(customerId, nextVisitedDoctorid, dateOfAppointment, startTime, endTime, isConfirmed, reasonForVisitId, appointmentNotes,StoreId ,doctorScheduleId);
        }

        [Authorize]
        public string Post(int customerId, int nextVisitedDoctorid, DateTime dateOfAppointment, string startTime, string endTime, bool isConfirmed, int reasonForVisitId, string appointmentNotes,int StoreId, int doctorScheduleId)
        {
            string msg = string.Empty;

            try
            {
                CreateAppointmentScheduleDataContext _Context = new CreateAppointmentScheduleDataContext();
                _Context.CreateAppointmentSchedule(customerId, nextVisitedDoctorid, dateOfAppointment, startTime, endTime, isConfirmed, reasonForVisitId, appointmentNotes,StoreId, doctorScheduleId);
                _Context.SubmitChanges();
                msg = "Appointment Created SuccessFully !";
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return msg;
        }
    }
}
