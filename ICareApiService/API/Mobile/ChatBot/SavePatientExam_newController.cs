﻿using ICareApiService.LinqToSql;
using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SavePatientExam_newController : ApiController
    {

        string optCmcustomerId;

        public void Get(string userName, int patientId, int doctorId, DateTime examDate, DateTime expireDate, string remarks, string attributedetails, int productId, int customerDocumentId, int storeId)
        {

            Post(userName, patientId, doctorId, examDate, expireDate, remarks, attributedetails, productId, customerDocumentId, storeId);
        }


        public string Post(string userName, int patientId, int doctorId, DateTime examDate, DateTime expireDate, string remarks, string attributedetails, int productId, int customerDocumentId, int storeID)
        {
            string msg = string.Empty;

            try
            {
                SavePatientExam_newDataContext _Context = new SavePatientExam_newDataContext();
              var result=_Context.SavePatientExam_new(patientId, doctorId, examDate, expireDate, remarks, attributedetails, productId, customerDocumentId);
                _Context.SubmitChanges();
                msg= result.FirstOrDefault().CustomerExamId.ToString();
            }
            catch (Exception ex)
            {
                msg = "Falied!"; //Error in save Rx
                return msg;
            }
            //OptiBuddy
            //GetCustomerIdByCustomerGuIdDataContext contest = new GetCustomerIdByCustomerGuIdDataContext();
            //try
            //{
            //    var customerId = contest.GetCustomerIdByCustomerGuId(userName);
            //    optCmcustomerId = customerId.FirstOrDefault().customerId.ToString();
            //}
            //catch (Exception ex)
            //{
            //    msg = "-1";
            //    return msg;
            //}
            //try
            //{
            //    int optcustomerId = Int32.Parse(optCmcustomerId);
            //    SaveCustomerRxExamOptcmrsDataContext opticmrsContest = new SaveCustomerRxExamOptcmrsDataContext();
            //    int? value = 0;
            //    if (value == 0)
            //    {
            //        value = null;
            //    }
            //    int? customer1examId = 0;

            //    if (customer1examId == 0)
            //    {
            //        customer1examId = null;
            //    }
            //    int CategoryId = 1;
            //    int ProductId = 0;
          
            //    opticmrsContest.SavePatientExam(optcustomerId, doctorId, examDate, expireDate, attributedetails, storeID, remarks, value, ref customer1examId, CategoryId, ProductId);
            //    opticmrsContest.SubmitChanges();
                
            //}
            //catch (Exception ex)
            //{
            //    msg = "Falied!";
            //}
            return msg;
        }
    }
}
