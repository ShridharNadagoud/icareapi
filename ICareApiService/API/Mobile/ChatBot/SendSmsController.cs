﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SendSmsController : ApiController
    {
        String message;

        public bool Get(string cusPhoneNumber, int casevalue, string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime)
        {
            return Post(cusPhoneNumber, casevalue,  doctorName,  customerName,  address,  city,  zipCode,  dateOfApp,  storeName,  storePhonenumber,  startTime);
        }
        public bool Post(string cusPhoneNumber, int casevalue, string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime)
        {
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                if (casevalue == 1)
                {
                     message = HttpUtility.UrlEncode("Dear" + customerName + ",  Your Appointment has been confirmed successfully !.Date :" + dateOfApp + "Time:" + startTime + " Practitioner :" + doctorName + " ,Location:" + address + "," + city + "," + zipCode + "." + " Please let us know if you have any questions.We look forward to seeing you then !." + storeName + ",Ph::" + storePhonenumber + ".");
                }
                else if(casevalue==2)
                {
                     message = HttpUtility.UrlEncode("Dear" + customerName + ",  Your Appointment has been Rescheduled successfully  !.Date :" + dateOfApp + "Time:" + startTime + " Practitioner :" + doctorName + " ,Location:" + address + "," + city + "," + zipCode + "." + " Please let us know if you have any questions.We look forward to seeing you then !." + storeName + ",Ph::" + storePhonenumber + ".");

                }
                else
                {
                    message = HttpUtility.UrlEncode("Dear" + customerName + ",  Your Appointment has been cancelled successfully" +
                        "" +
                        "" +
                        "" +
                        " !.Date :" + dateOfApp + "Time:" + startTime + " Practitioner :" + doctorName + " ,Location:" + address + "," + city + "," + zipCode + "." + " Please let us know if you have any questions.We look forward to seeing you then !." + storeName + ",Ph::" + storePhonenumber + ".");

                }
                using (var wb = new WebClient())
                {
                    byte[] response = wb.UploadValues("https://api.textlocal.in/send/", new NameValueCollection()
                {
                {"apikey" , "lsCva7URzlY-DqEiSrm5feDEMbYBTPcJoW6WePS3g7"},
                {"numbers" , "91"+cusPhoneNumber},
                {"message" , message},
                {"sender" , "TXTLCL"}
                });
                    string result = System.Text.Encoding.UTF8.GetString(response);
                    return true;

                }

            }

        }
    }
}

