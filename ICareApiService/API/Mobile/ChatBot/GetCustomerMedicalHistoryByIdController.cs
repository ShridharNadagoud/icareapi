﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetCustomerMedicalHistoryByIdController : ApiController
    {
        GetCustomerMedicalHistoryByIdDataContext context = new GetCustomerMedicalHistoryByIdDataContext();
        public List<GetCustomerMedicalHistoryByIdResult> Get(string customerId)
            {
            var result = context.GetCustomerMedicalHistoryById(customerId);
            return result.ToList();
            }
    }
}
