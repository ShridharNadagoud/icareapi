﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetStoreEmailIDByCompanyIDController : ApiController
    {
        GetStoreEmailIDByCompanyIDDataContext context = new GetStoreEmailIDByCompanyIDDataContext();
        public List<GetStoreEmailIDByCompanyIDResult> Get(string companyId)
        {
            var resultList = context.GetStoreEmailIDByCompanyID(companyId);
            return resultList.ToList();
        }
    }
}
