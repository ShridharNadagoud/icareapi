﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SaveDoctorDPController : ApiController
    {
        SaveDoctorDPDataContext context = new SaveDoctorDPDataContext();
        public int Get(int doctorId, int companyId, string companyRefCode)
        {
           return Post(doctorId, companyId, companyRefCode);
        }
        public int Post(int DoctorId,int companyId,string companyRefCode)
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string fileName = string.Empty, fileContentType = string.Empty, singlefilePathName = string.Empty;
                byte[] document = null;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                       
                        fileName = postedFile.FileName;
                        if (fileName.Contains("\\") || fileName.Contains(@"\"))
                        {
                            fileName = postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault();
                        }
                        var data = fileName.Split('.');//postedFile.ContentType;
                        
                        fileContentType = data.LastOrDefault();
                        Stream str = postedFile.InputStream;
                        BinaryReader Br = new BinaryReader(str);
                        document = Br.ReadBytes((Int32)str.Length);

                        string filePath = HttpContext.Current.Server.MapPath("~");
                        string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString().ToLower();
                        string singlefilePath = "/assets/" + "files/" + controllerName + "/";
                        singlefilePathName = singlefilePath + fileName;
                        string newfilePAth = filePath.Replace("\\", "/") + singlefilePath;
                        //if (!Directory.Exists(newfilePAth))
                        //{
                        //    Directory.CreateDirectory(newfilePAth);
                        //    File.WriteAllBytes(@newfilePAth + fileName, document);
                        //}
                        //else
                        //{
                        //    File.WriteAllBytes(@newfilePAth + fileName, document);
                        //}
                    }
                    var result = context.SaveDoctorDP(DoctorId, companyId, companyRefCode, document, fileName, fileContentType, singlefilePathName);
                    return (int)result.FirstOrDefault().DoctorDisplayProfileId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }
    }
}
