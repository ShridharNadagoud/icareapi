﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class CheckAppointmentSlotController : ApiController
    {
        public string Get(int CustomerID,int NextVisitedDoctorID,DateTime DateofAppointment,string StartTime)
        {
            CheckAppointmentSlotDataContext _context = new CheckAppointmentSlotDataContext();
            try
            {
                var result = _context.CheckAppointmentSlot(CustomerID, NextVisitedDoctorID, DateofAppointment, StartTime);
                if (result != null)
                {
                    var resultData = result.FirstOrDefault();
                    if (resultData != null)
                    {
                        var appointmentId = resultData.AppointmentId;
                        if (appointmentId > 0)
                        {
                            return "1"; //Slot Booked
                        }
                        else
                        {
                            return "0"; //Slot free
                        }
                    }
                    else
                    {
                        return "0";//slot free
                    }
                }
                else
                {
                    return "0";//slot free
                }

            }
            catch (Exception ex)
            {
                return ex.ToString();//No Data
            }
        }
    }
}
