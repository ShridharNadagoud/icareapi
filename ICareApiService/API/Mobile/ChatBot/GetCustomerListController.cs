﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetCustomerListController : ApiController
    {
        GetAllCustomersByCompanyIdDataContext _Context = new GetAllCustomersByCompanyIdDataContext();

        [Authorize]
        public List<GetAllCustomerDetailsResult> Get(int id)
        {
            var customerList = _Context.GetAllCustomerDetails(id);
            return customerList.ToList();
        }
    }
}
