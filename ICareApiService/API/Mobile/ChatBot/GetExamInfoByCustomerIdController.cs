﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetExamInfoByCustomerIdController : ApiController
    {


        GetExamInfoByCustomerIdDataContext _Context = new GetExamInfoByCustomerIdDataContext();

        //[Authorize]
        public List<GetExamInfoByCustomerIdResult> Get(int id)
        {
            var result = _Context.GetExamInfoByCustomerId(id);

            return result.ToList();
        }


    }
}