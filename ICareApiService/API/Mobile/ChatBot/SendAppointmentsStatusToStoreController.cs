﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SendAppointmentsStatusToStoreController : ApiController
    {
        string htmlBody;

        public int Get(string storeEmailId, int _case, string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime)
        {
            return Post(storeEmailId, _case, doctorName, customerName, address, city, zipCode, dateOfApp, storeName, storePhonenumber, startTime);
        }

        public int Post(string storeEmailId,int _case, string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime)
        {
            customerName= customerName.First().ToString().ToUpper() + customerName.Substring(1);
            doctorName = doctorName.First().ToString().ToUpper() + doctorName.Substring(1);
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials =
            new System.Net.NetworkCredential("optirisesoftwares@gmail.com", "UsiIcare@54321");
            SmtpServer.EnableSsl = true;
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("optirisesoftwares@gmail.com");
                mail.To.Add(storeEmailId);
               // mail.CC.Add(cusEmailId);
                mail.Subject = "Appointment Status";
                int typeofcase = _case;
                if (_case == 1)
                {
                    string BodyOfHtml = AppointmentBody(doctorName, customerName, address, city, zipCode, dateOfApp, storeName, storePhonenumber, startTime);

                    //mail.Body = "Appointment booked has been Confimed";
                    mail.Body = BodyOfHtml;
                }
                else if (_case == 2)
                {
                    string BodyOfHtml = ApmtRescheduledBody(doctorName, customerName, address, city, zipCode, dateOfApp, storeName, storePhonenumber, startTime);

                    mail.Body = BodyOfHtml;
                }
                else
                {
                    string BodyOfHtml = ApmtCancledBody(doctorName, customerName, address, city, zipCode, dateOfApp, storeName, storePhonenumber, startTime);

                    mail.Body = BodyOfHtml;
                }
                mail.IsBodyHtml = true;
                SmtpServer.Send(mail);
                return 1; //Successfully send
            }
            catch
            {
                return -1; //Email unsuccessful
            }
        }

        private string ApmtCancledBody(string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime)
        {
            htmlBody = "<div id=dvContainer class= modal - body>";
            htmlBody += "<p>Dear  &nbsp;<b> Admin </b>,<br/><br/><i> Appointment has been Canceled  !.</i><br><h4>When </h4>";
            htmlBody += "<i>" + dateOfApp + "&nbsp; &nbsp;" + startTime + "</i><br/><h4>Practitioner :</h4>";
            htmlBody += "<i>" + doctorName + "</i><br/>";
            htmlBody += "<h4>Customer :</h4>";
            htmlBody += "<i>" + customerName + "</i><br/><br/>";
            htmlBody += "<b>Location</b><br/><i>" + address + "</i><br/><i>" + city + "</i><br/><i>" + zipCode + "</i><br/></p><br/>";
            htmlBody += "<p><i>Please let us know if you have any questions.We look forward to seeing you then !.</i><br>";
            htmlBody += "<b><i id='printStoreName'>" + storeName + "</i>,</b><br>";
            htmlBody += "<b><i>Ph:</i><i id ='printStorePhoneNumber'>" + storePhonenumber + "</i></b></p></div>";
            htmlBody += @"<div class='modal - footer' style='text - align:center'><div class=row></div></div>";
            return htmlBody;
        }

        private string ApmtRescheduledBody(string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime)
        {
            htmlBody = "<div id=dvContainer class= modal - body>";
            htmlBody += "<p>Dear  &nbsp;<b id =printCustomerName >Admin</b>,<br/><br/><i>Appointment has been Rescheduled  !.</i><br><h4>When</h4>";
            htmlBody += "<i>" + dateOfApp + "&nbsp; &nbsp;" + startTime + "</i><br/><h4>Practitioner :</h4>";
            htmlBody += "<i>" + doctorName + "</i><br/>";
            htmlBody += "<h4>Customer :</h4>";
            htmlBody += "<i>" + customerName + "</i><br/><br/>";
            htmlBody += "<b>Location</b><br/><i>" + address + "</i><br/><i>" + city + "</i><br/><i>" + zipCode + "</i><br/></p><br/>";
            htmlBody += "<p><i>Please let us know if you have any questions.We look forward to seeing you then !.</i><br>";
            htmlBody += "<b><i id='printStoreName'>" + storeName + "</i>,</b><br>";
            htmlBody += "<b><i>Ph:</i><i id ='printStorePhoneNumber'>" + storePhonenumber + "</i></b></p></div>";
            htmlBody += @"<div class='modal - footer' style='text - align:center'><div class=row></div></div>";
            return htmlBody;

        }

        private string AppointmentBody(string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime)
        {
            htmlBody = "<div id=dvContainer class= modal - body>";
            htmlBody += "<p>Dear  &nbsp;<b id =printCustomerName > Admin</b>,<br/><br/><i>Appointment has been confirmed !.</i><br><h4>When </h4>";
            htmlBody += "<i>" + dateOfApp + "&nbsp; &nbsp;" + startTime + "</i><br/><h4>Practitioner :</h4>";
            htmlBody += "<i>" + doctorName + "</i><br/>";
            htmlBody += "<h4>Customer :</h4>";
            htmlBody += "<i>" + customerName + "</i><br/><br/>";
            htmlBody += "<b>Location</b><br/><i>" + address + "</i><br/><i>" + city + "</i><br/><i>" + zipCode + "</i><br/></p><br/>";
            htmlBody += "<p><i>Please let us know if you have any questions.We look forward to seeing you then !.</i><br>";
            htmlBody += "<b><i id='printStoreName'>" + storeName + "</i>,</b><br>";
            htmlBody += "<b><i>Ph:</i><i id ='printStorePhoneNumber'>" + storePhonenumber + "</i></b></p></div>";
            htmlBody += @"<div class='modal - footer' style='text - align:center'><div class=row></div></div>";
            return htmlBody;
        }
    }
}
