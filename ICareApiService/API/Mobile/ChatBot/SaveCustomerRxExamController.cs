﻿using ICareApiService.LinqToSql;
using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SaveCustomerRxExamController : ApiController
    {
        string optCmcustomerId;

        public void Get(string userName,int patientId, int doctorId, DateTime examDate, DateTime expireDate, string remarks, string attributedetails, int productId, int customerDocumentId,int storeId)
        {

            Post(userName,patientId, doctorId, examDate, expireDate, remarks, attributedetails, productId, customerDocumentId,storeId);
        }


        public string Post(string userName,int patientId, int doctorId, DateTime examDate, DateTime expireDate, string remarks, string attributedetails, int productId, int customerDocumentId,int storeID)
        {
            string msg = string.Empty;
             
                try
                {
                    SaveCustomerExamDataContext _Context = new SaveCustomerExamDataContext();
                    _Context.SavePatientExam(patientId, doctorId, examDate, expireDate, remarks, attributedetails, productId, customerDocumentId);
                    _Context.SubmitChanges();
                }catch(Exception ex)
                {
                    msg = "Falied!"; //Error in save Rx
                return msg;
                }
                GetCustomerIdByCustomerGuIdDataContext contest = new GetCustomerIdByCustomerGuIdDataContext();
                try
                {
                    var customerId = contest.GetCustomerIdByCustomerGuId(userName);
                     optCmcustomerId = customerId.FirstOrDefault().customerId.ToString();
                }catch(Exception ex)
                {
                    msg = "-1";
                return msg;
                }
                try
                {
                    int optcustomerId = Int32.Parse(optCmcustomerId);
                    SaveCustomerRxExamOptcmrsDataContext opticmrsContest = new SaveCustomerRxExamOptcmrsDataContext();
                    int? value = 0;
                    if (value == 0)
                    {
                        value = null;
                    }
                    int? customer1examId = 0;

                    if (customer1examId == 0)
                    {
                        customer1examId = null;
                    }
                int CategoryId = 1;
                int ProductId=0;
                    opticmrsContest.SavePatientExam(optcustomerId, doctorId, examDate, expireDate, attributedetails, storeID, remarks, value, ref customer1examId, CategoryId, ProductId);
                    opticmrsContest.SubmitChanges();
                    msg = "Saved !";
                }catch(Exception ex)
                {
                msg = "Falied!";
                }
                    
                
            return msg;
        }
    }
}
