﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ICareApiService.LinqToSql.Mobile.ChatBot;
namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetAppointmentDetailByDoctorIdController : ApiController
    {
        GetAppointmentDetailByDoctorIdDataContext _Context = new GetAppointmentDetailByDoctorIdDataContext();

        [Authorize]
        public List<GetAppointmentDetailsByDoctorIdResult> Get(int id)
        {
            var appointmentList = _Context.GetAppointmentDetailsByDoctorId(id);
            return appointmentList.ToList();
        }
    }
}
