﻿using ICareApiService.LinqToSql;
using ICareApiService.LinqToSql.Mobile.ChatBot;
using ICareApiService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SaveOrUpdateAppointmentController : ApiController
    {
        GetCustomerIdDataContext _context = new GetCustomerIdDataContext();


        [Authorize]
        public void Post(object appointmentDetails)
        {
            try
            {
                SaveOrUpdateAppointment saveOrUpdateAppointment = new SaveOrUpdateAppointment();
                saveOrUpdateAppointment = JsonConvert.DeserializeObject<SaveOrUpdateAppointment>(appointmentDetails.ToString());

                var custId = !String.IsNullOrEmpty(saveOrUpdateAppointment.CustomerId) ? Convert.ToInt32(saveOrUpdateAppointment.CustomerId) : _context.SP_GetCustomerId(Convert.ToInt32(saveOrUpdateAppointment.StoreId), saveOrUpdateAppointment.CustomerFirstName, saveOrUpdateAppointment.CustomerLastName, saveOrUpdateAppointment.CustomerDob, Convert.ToDecimal(saveOrUpdateAppointment.CustomerPhoneNumber), saveOrUpdateAppointment.CustomerEmailId);
                if (custId != 0)
                {
                    int? appointmentId = saveOrUpdateAppointment.AppointmentId;
                    int storeId = Convert.ToInt32(saveOrUpdateAppointment.StoreId);

                    TimeSpan endTime = DateTime.Parse(saveOrUpdateAppointment.TimeScheduled).TimeOfDay;
                    TimeSpan time = saveOrUpdateAppointment.Duration != "0" ? TimeSpan.FromMinutes(Convert.ToInt32(saveOrUpdateAppointment.Duration)) : TimeSpan.FromMinutes(30);
                    var result = _context.SP_Save_AppointmentDetail(appointmentId, storeId, saveOrUpdateAppointment.DoctorId, saveOrUpdateAppointment.ReasonForVisitId
                        , null, custId, saveOrUpdateAppointment.DateScheduled.Date, saveOrUpdateAppointment.TimeScheduled
                        , endTime.Add(time).ToString(), saveOrUpdateAppointment.Notes, saveOrUpdateAppointment.CustomerPhoneNumber
                        , null, true, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ref appointmentId);
                }

            }
            catch (Exception ex)
            {

            }
        }
    }
}
