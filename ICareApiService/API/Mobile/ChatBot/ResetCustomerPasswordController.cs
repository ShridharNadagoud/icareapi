﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class ResetCustomerPasswordController : ApiController
    {
        public void Get(string userName, string oldPassword, string newPassword)
        {
            Post(userName, oldPassword, newPassword);
        }

        [Authorize]
        public int Post( string userName, string oldPassword, string newPassword)
        {
            try
            {
                ResetCustomerPasswordDataContext _context = new ResetCustomerPasswordDataContext();
                var result = _context.ResetCustomerPassword( userName, oldPassword, newPassword);
                if (result == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
              
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
