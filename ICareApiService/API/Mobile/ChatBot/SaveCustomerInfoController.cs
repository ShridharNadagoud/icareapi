﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SaveCustomerInfoController : ApiController
    {
        [Authorize]
        // GET: api/SaveCustomerInfo
        public string Get(string firstName, string lastName, string phoneNumber, DateTime dateOfBirth, string emailId, string Gender, int age, int doctorId, string address1, string address2, string city, string state, string zipCode, string country)
        {
            var id = Post(firstName, lastName, phoneNumber, dateOfBirth, emailId, Gender, age, doctorId, address1, address2, city, state, zipCode, country);
            return id;
        }

        // POST: api/SaveCustomerInfo
        [Authorize]
        [ActionName("Post")]
        public string Post(string firstName, string lastName, string phoneNumber, DateTime dateOfBirth, string emailId, string Gender, int age, int doctorId, string address1, string address2, string city, string state, string zipCode, string country)
        {
            try
            {
                string password = string.Empty;
                SaveCustomerInfoDataContext _Context = new SaveCustomerInfoDataContext();
                var customers = _Context.SaveCustomerInfo(firstName, lastName, phoneNumber, dateOfBirth, emailId, Gender, age, doctorId, address1, address2, city, state, password, zipCode, country);
                _Context.SubmitChanges();
                var customer = customers.FirstOrDefault();
                return customer.ID.ToString();
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }

        [ActionName("Post")]
        public string Post(string firstName, string lastName, string phoneNumber, string emailId, string password, DateTime dateOfBirth)
        {
            //DateTime dateOfBirth = DateTime.Now;
            string address1 = string.Empty, city = string.Empty, Gender = string.Empty, address2 = string.Empty, state = string.Empty, zipCode = string.Empty, country = string.Empty;
                ;
            int age = 0, doctorId = 0;
            try
            {
                IsExistCustomerUserNameDataContext _customerNamecontext = new IsExistCustomerUserNameDataContext();
                var data = _customerNamecontext.IsExistCustomerUserName(emailId,firstName,dateOfBirth);
                string isCustomerExist = data.FirstOrDefault().UserAvailable;
                if (isCustomerExist == "0")
                {                    
                    try
                    {
                        SaveCustomerInfoResult _contestResult = new SaveCustomerInfoResult();
                        SaveCustomerInfoDataContext _Context = new SaveCustomerInfoDataContext();
                  var saveCustomerInfo=_Context.SaveCustomerInfo(firstName, lastName, phoneNumber, dateOfBirth, emailId, Gender, age, doctorId, address1, address2, city, state, password, zipCode, country);
                        //_Context.SubmitChanges();
                        _contestResult = saveCustomerInfo.FirstOrDefault();
                        //var customer = _Context.Customers.Where(x => x.FirstName == firstName && x.LastName == lastName && x.EmailID == emailId && x.PhoneNumber == phoneNumber && x.Password == password).FirstOrDefault();
                        //string resultCustomerId = customer.ID.ToString();
                        //string resultPassword = customer.Password.ToString();
                        if (_contestResult != null)
                        {
                            return "0";
                        }
                         
                        else
                        {
                            return "1"; //Customer ID not available
                        }
                    }
                    catch (Exception ex)
                    {
                        return "Failed";
                    }
                }
                else
                {
                    try
                    {
                        UpdateCustomerPasswordDataContext update = new UpdateCustomerPasswordDataContext();
                        update.UpdateCustomerPassword(emailId, password,firstName,dateOfBirth);
                        return "UpdatedSuccessfuly";
                    }
                    catch
                    {
                        return "Failed";
                    }
                }
            }
            catch
            {
                return "Failed";
            }         
        }
    }
}