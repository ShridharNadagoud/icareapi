﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetCustomerIdByCustomerUIdController : ApiController
    {
        GetCustomerIdByCustomerUIdDataContext contest = new GetCustomerIdByCustomerUIdDataContext();

        public string Get(string CuId)
        {
            try
            {
                var customerId = contest.GetCustomerIdByCustomerUId(CuId);
                return customerId.FirstOrDefault().customerId.ToString();
            }
            catch (Exception ex)
            {
                return "1";
            }
        }
    }
}
