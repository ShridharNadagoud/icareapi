﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetReasonForVisitController : ApiController
    {
        GetReasonForVisitDataContext context = new GetReasonForVisitDataContext();
        public List<GetReasonForVisitResult> Get()
        {
            var result = context.GetReasonForVisit();
            return result.ToList();
        }
    }
}
