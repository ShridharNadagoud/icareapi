﻿using ICareApiService.LinqToSql.Mobile;
using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class IsExistUserNameController : ApiController
    {
        IsExistUserNameDataContext _context = new IsExistUserNameDataContext();
        public bool Get(string userName)
        {
            var result = _context.IsExistUserName(userName);
            if (result.FirstOrDefault().UserAvailable == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}