﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class UpdateCustomerInfoController : ApiController
    {
         public string Get(int customerId,string firstName,string lastName,string phoneNumber,DateTime DateOfBirth, string Gender,int Age ,string Address,string City,string State,string ZipCode,string addresstwo,string cuscountry)
	 
        {
          var id = Post(customerId, firstName, lastName, phoneNumber, DateOfBirth, Gender, Age, Address, City, State, ZipCode, addresstwo,cuscountry);
            return id;
        }

        // POST: api/UpdateCustomerInfo
        [ActionName("Post")]
        public string Post(int customerId, string firstName,string lastName,string phoneNumber,DateTime DateOfBirth,string Gender,int Age, string Address, string City, string State, string ZipCode,string addresstwo,string cuscountry)
        {
           
            try
            {
                UpdateCustomerInfoDataContext _context = new UpdateCustomerInfoDataContext();
                //UpdateCustomerInfoOpticmDataContext content = new UpdateCustomerInfoOpticmDataContext();
               GetCustomerIdBaseOnGuIDDataContext getIdContext = new GetCustomerIdBaseOnGuIDDataContext();
                var result = _context.UpdateCustomerInfo(customerId, firstName, lastName, phoneNumber, DateOfBirth, Gender, Age, Address,addresstwo, City, State, ZipCode,cuscountry);
                var customerGuid= result.FirstOrDefault().CustomerUid.ToString();
                //var cusID = getIdContext.GetCustomerIdBaseOnGuID(customerGuid);
                //var ctID = cusID.FirstOrDefault().customerId.ToString();
                //int cID = Int32.Parse(ctID);
                //var request = content.UpdateCustomerInfoOpticm(cID, firstName, lastName, phoneNumber, DateOfBirth, Gender, Address, City, ZipCode);
                
                return "1";
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
       
        //[ActionName("Post")]
        //public int Post(string UserName, string Password, string firstName, string lastName, string phoneNumber)
        //{
        //    DateTime DateOfBirth=DateTime.Now; string Gender=null; int? Age=null; 
        //    try
        //    {
        //        UpdateCustomerInfoDataContext _context = new UpdateCustomerInfoDataContext();
        //        var result = _context.UpdateCustomerInfo(UserName, Password, firstName, lastName, phoneNumber, DateOfBirth, Gender, Age);
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return -1;
        //    }
        //}
    }
}
