﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class UpdatePasswordController : ApiController
    {

        public void Get(string companyRefCode, string userName,string oldPassword, string newPassword)
        {
            Post(companyRefCode, userName, oldPassword,newPassword);
        }


        public int Post(string companyRefCode, string userName,string oldPassword, string newPassword)
        {
            try
            {
                UpdatePasswordDataContext _context = new UpdatePasswordDataContext();
                var result = _context.UpdatePassword(companyRefCode, userName,oldPassword, newPassword);
                return result;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
    }
}