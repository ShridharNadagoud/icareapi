﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SendRemainderSMSController : ApiController
    {
        string message;
        public bool Get(string docEmailId, string cusEmailId, int _case, string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime, string customerPhoneNumber)
        {
            return Post(docEmailId, cusEmailId, _case, doctorName, customerName, address, city, zipCode, dateOfApp, storeName, storePhonenumber, startTime, customerPhoneNumber);
        }

        public bool Post(string docEmailId, string cusEmailId, int _case, string doctorName, string customerName, string address, string city, string zipCode, string dateOfApp, string storeName, string storePhonenumber, string startTime, string customerPhoneNumber)
        {
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                if (_case == 1)
                {
                    message = HttpUtility.UrlEncode("Dear" + customerName + ",  Your Appointment has been confirmed successfully !.Date :" + dateOfApp + "Time:" + startTime + " Practitioner :" + doctorName + ".");
                }
                else if (_case == 2)
                {
                    message = HttpUtility.UrlEncode("Dear" + customerName + ",  Your Appointment has been Rescheduled successfully  !.Date :" + dateOfApp + "Time:" + startTime + " Practitioner :" + doctorName + ".");

                }
                else
                {
                    message = HttpUtility.UrlEncode("Dear" + customerName + ",  Your Appointment has been cancelled successfully" + " " +
                        "" +
                        "" +
                        " !.Date :" + dateOfApp + "Time:" + startTime + " Practitioner :" + doctorName + ".");

                }
                using (var wb = new WebClient())
                {
                    byte[] response = wb.UploadValues("https://api.textlocal.in/send/", new NameValueCollection()
                {
                {"apikey" , "lsCva7URzlY-DqEiSrm5feDEMbYBTPcJoW6WePS3g7"},
                {"numbers" , "91"+customerPhoneNumber},
                {"message" , message},
                {"sender" , "TXTLCL"}
                });
                    string result = System.Text.Encoding.UTF8.GetString(response);
                    return true;

                }
            }
        }
    }
}
