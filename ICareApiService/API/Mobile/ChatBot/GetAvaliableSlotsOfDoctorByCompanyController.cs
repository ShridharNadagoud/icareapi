﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetAvaliableSlotsOfDoctorByCompanyController : ApiController
    {
        GetAvaliableSlotsOfDoctorByCompanyDataContext _Context = new GetAvaliableSlotsOfDoctorByCompanyDataContext();

        [Authorize]
        public List<SP_GetAvailableAppointmentsResult> Get(string companyRefCode, string storeId, string doctorId, DateTime apptStartDate, DateTime apptEndDate)
        {
            try
            {
                var availableAppointmentList = _Context.SP_GetAvailableAppointments(!String.IsNullOrEmpty(companyRefCode) ? companyRefCode : null, Convert.ToInt32(storeId), Convert.ToInt32(doctorId), apptStartDate, apptEndDate);
                return availableAppointmentList.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
        }
    }
}
