﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class ChangeAppointmentStatusController : ApiController
    {

        ChangeAppointmetStatusDataContext _Context = new ChangeAppointmetStatusDataContext();

        [Authorize]
        public void POST(string apptId, string apptSatusId)
        {
            _Context.SP_ChangeAppointmentStatus(Convert.ToInt32(apptId), Convert.ToInt32(apptSatusId));
           
        }
    }
}
