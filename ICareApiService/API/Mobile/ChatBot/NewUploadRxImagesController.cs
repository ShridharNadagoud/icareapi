﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class NewUploadRxImagesController : ApiController
    {
        int CustomerDocumentId;
        int i = 0;
        UpdateRxImage_newDataContext context = new UpdateRxImage_newDataContext();
        public int Get(string ImageTemp ,int customerExamId, int CustomerId)
        {
            return Post(ImageTemp,customerExamId, CustomerId);
        }
        public int Post(string ImageTemp, int customerExamId, int CustomerId)
        {
            try
            {

                string[] stringList = null;
                char[] splitchar = {'|'};
                stringList = ImageTemp.Split(splitchar);

                for (i = 0; i < stringList.Length-1; i++)
                {
                    if (stringList[i].Count()!=0)
                    {
                        string custDcmntId = stringList[i];
                        int intcusDomntId = Int32.Parse(custDcmntId);
                        CustomerDocumentId = intcusDomntId;
                        var result = context.UpdateRxImage_new(customerExamId, CustomerId, CustomerDocumentId);
                    }

                }
                return 0;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
    }
}
