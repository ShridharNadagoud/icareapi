﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetDoctorDisplayImageController : ApiController
    {
        GetDoctorDisplayImageDataContext _context = new GetDoctorDisplayImageDataContext();
        public string Get(int id)
        {
            var data = _context.GetDoctorDisplayImage(id);
            return data.FirstOrDefault().FilePath;
        }
    }
}
