﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Web;
using File = Google.Apis.Drive.v2.Data.File;
using IFile = Google.Apis.Drive.v3.Data.File;

using System.Web.Http;
using Google.Apis.Drive.v3.Data;
using iTextSharp.text;
 

namespace ICareApiService.API.Mobile.ChatBot
{
    public class TestImageResizeController : ApiController
    {
        System.Drawing.Image localimage;
        byte[] document = null;
        string fileId;
        string returnId;
        string googlepath;
        static string[] scopes = { DriveService.Scope.Drive };
        SaveDoctorDPDataContext context = new SaveDoctorDPDataContext();
        public string Get(int doctorId, int companyId, string companyRefCode)
        {
            return Post(doctorId, companyId, companyRefCode);
        }
        public string Post(int DoctorId, int companyId, string companyRefCode)
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string fileName = string.Empty, fileContentType = string.Empty, singlefilePathName = string.Empty;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        fileName = postedFile.FileName;
                        if (fileName.Contains("\\") || fileName.Contains(@"\"))
                        {
                            fileName = postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault();
                        }
                        var data = fileName.Split('.');//postedFile.ContentType;
                        fileContentType = data.LastOrDefault();
                        Stream str = postedFile.InputStream;
                        BinaryReader Br = new BinaryReader(str);
                       // document = Br.ReadBytes((Int32)str.Length);
                        document = null;
                        localimage = System.Drawing.Image.FromStream(str);
                        googlepath = Path.Combine(HttpContext.Current.Server.MapPath("~/GoogleDrive/" + postedFile.FileName));
                        string resized = ResizeImage(str, new Size(3264, 2440), googlepath);
                        returnId = FileUpload(postedFile, resized);
                        singlefilePathName = "https://drive.google.com/thumbnail?id="+returnId;

                        var result = context.SaveDoctorDP(DoctorId, companyId, companyRefCode, document, fileName, fileContentType, singlefilePathName);
                        //Delete Temp Images
                        var deletePath = HttpContext.Current.Server.MapPath("~/GoogleDrive/");
                        string[] filePaths = Directory.GetFiles(deletePath);
                        foreach (string filePath in filePaths)
                            System.IO.File.Delete(filePath);

                        returnId = result.FirstOrDefault().DoctorDisplayProfileId.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return returnId;
        }

        //Upload Image into Google Drive 
        private string FileUpload(HttpPostedFile postedFile, string resizeimg)
        {
            if (postedFile != null && postedFile.ContentLength > 0)
            {
                
                DriveService service = GetService();
                var folderId = "1Eu7SCR8Pfep_HiLhK_GbN-Nt88IyjxE7";
                var fileMetadata = new IFile()
                {
                    Name = "photo.jpg",
                    Parents = new List<string>
    {
        folderId
    }
                };

                var FileMetaData = new Google.Apis.Drive.v3.Data.File();
                FileMetaData.Name = Path.GetFileName(resizeimg);
                FileMetaData.MimeType = GetMimeType(resizeimg);
                FilesResource.CreateMediaUpload request;

                using (var stream = new System.IO.FileStream(googlepath, System.IO.FileMode.Open))
                {
                    request = service.Files.Create(fileMetadata, stream, FileMetaData.MimeType);
                    request.Fields = "id";
                    request.Upload();
                    

                }
                var filename = request.ResponseBody;
                fileId = filename.Id;

            }

            return fileId;
        }

        private static string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
         
        //Compress Image
        private string ResizeImage(Stream localimage, Size size, string targetPath)
        {

            int originalWidth = size.Width;
            int originalHeight = size.Height;
            float percentWidth = (float)size.Width / (float)originalWidth;
            float percentHeight = (float)size.Height / (float)originalHeight;
            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;

            // Image newImage = new Bitmap(newWidth, newHeight);
            using (var image = System.Drawing.Image.FromStream(localimage))
            {

                // var newWidth = (int)(originalWidth *);
                //var newHeight = (int)(originalHeight * );

                var newWidth = (int)(image.Width * 0.3);
                var newHeight = (int)(image.Height * 0.3);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.PixelOffsetMode = PixelOffsetMode.HighQuality;
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                //thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
            return targetPath;
        }

        //Get Athentication of Google drive Account
        public static DriveService GetService()
        {
            UserCredential credential;
            string[] scopes = new string[] { DriveService.Scope.Drive }; // Full access
 
            using (var stream = new FileStream(HttpContext.Current.Server.MapPath("~/GoogleDriveCredentials/client_secret.json"), FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, HttpContext.Current.Server.MapPath("~/GoogleDriveCredentials/"));
                var ClientId = "98069384036-6q08kc8oekpc111q4nte8mj1hvvaci6d.apps.googleusercontent.com";
                var ClientSecret = "Px5WUJR6DIffeEC7wjBmz01p";
                var clientSecrets = new ClientSecrets() { ClientId = ClientId, ClientSecret = ClientSecret };
                credential = GoogleWebAuthorizationBroker
                               .AuthorizeAsync(
                    clientSecrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(HttpContext.Current.Server.MapPath("~/GoogleDriveCredentials/"), true)
                    ).Result;
            }

            
            var services = new DriveService(new BaseClientService.Initializer()
            {
                //ApiKey = "AIzaSyCLN7bo4NHpEQZ04IhJuS1C5v5rIjv5JhE",  // from https://console.developers.google.com (Public API access)
                HttpClientInitializer = credential,

                ApplicationName = "ICareApiService",
            });
            //.https://drive.google.com/drive/folders/1Eu7SCR8Pfep_HiLhK_GbN-Nt88IyjxE7?usp=sharing
            return services;
        }
    }
}


