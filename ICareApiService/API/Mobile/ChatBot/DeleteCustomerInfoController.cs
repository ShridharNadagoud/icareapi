﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class DeleteCustomerInfoController : ApiController
    {
        public int Get(int customerId)

        {
            var id = Post(customerId);
            return id;
        }

        // POST: api/UpdateCustomerInfo
        [ActionName("Post")]
        public int Post(int customerId)
        {

            try
            {
                DeleteCustomerInfoDataContext _context = new DeleteCustomerInfoDataContext();
                var result = _context.DeleteCustomerInfo(customerId);
                return result;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
