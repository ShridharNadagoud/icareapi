﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using ICareApiService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetApptDetailByCompanyByStoreByDoctorController : ApiController
    {
        GetBookedAppointmentDetailsDataContext _Context = new GetBookedAppointmentDetailsDataContext();
        [Authorize]
        public List<SP_GetBookedAppointmentsResult> Get(string companyRefCode, string storeId, string doctorId, string apptStartDate, string apptEndDate, string apptStatusId)
        {
            GetAppointmentDetailsParams getAppointmentDetailsParams = new GetAppointmentDetailsParams();
            getAppointmentDetailsParams.storeId = !String.IsNullOrEmpty(storeId) ? Convert.ToInt32(storeId) : getAppointmentDetailsParams.storeId;
            getAppointmentDetailsParams.doctorId = !String.IsNullOrEmpty(doctorId) ? Convert.ToInt32(doctorId) : getAppointmentDetailsParams.doctorId;
            getAppointmentDetailsParams.apptStartDate = !String.IsNullOrEmpty(apptStartDate) ? Convert.ToDateTime(apptStartDate) : getAppointmentDetailsParams.apptStartDate;
            getAppointmentDetailsParams.apptEndDate = !String.IsNullOrEmpty(apptEndDate) ? Convert.ToDateTime(apptEndDate) : getAppointmentDetailsParams.apptEndDate;
            getAppointmentDetailsParams.apptStatusId = !String.IsNullOrEmpty(apptStatusId) ? Convert.ToInt32(apptStatusId) : getAppointmentDetailsParams.apptStatusId;
            var appointmentList = _Context.SP_GetBookedAppointments(companyRefCode, getAppointmentDetailsParams.storeId, getAppointmentDetailsParams.doctorId, getAppointmentDetailsParams.apptStartDate, getAppointmentDetailsParams.apptEndDate, getAppointmentDetailsParams.apptStatusId);
            //var apptId = appointmentList.Select(x=> x.AppointmentID).ToList();
            return appointmentList.ToList();
        }
    }
}
