﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using ICareApiService.Models;
using ICareApiService.Service;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class CreateRegistrationInformationController : ApiController
    {
        string msg = "";
        // GET: api/CreateRegistrationInformation/5

        public void Get(string companyName, string storeAddress1, string storeAddress2, string storeCity, string storePinCode, string storeState, string storeCountry, string DoctorFirstName, string DoctorLastName, char Gender, DateTime DOB, string PhoneNumber, string UserName, string Password)
        {
            Post(companyName, storeAddress1, storeAddress2, storeCity, storePinCode, storeState, storeCountry, DoctorFirstName, DoctorLastName, Gender, DOB, PhoneNumber, UserName, Password);
        }

        //public void Get(string companyRefCode, string DoctorFirstName, string DoctorLastName, char Gender, DateTime DOB, string PhoneNumber, string UserName, string Password)
        //{
        //       //companyRefCode, DoctorFirstName, DoctorLastName, Gender, DOB, PhoneNumber, UserName, Password,storeAddress1,storeAddress2,storeCity,storePinCode,storeState,storeCountry
        //    Post(companyRefCode, DoctorFirstName, DoctorLastName, Gender, DOB, PhoneNumber, UserName, Password,storeAddress1,storeAddress2,storeCity,storePinCode,storeState,storeCountry);
        //}

        // POST: api/CreateRegistrationInformation
        //new Company

        public string Post(string companyName, string storeAddress1, string storeAddress2, string storeCity, string storePinCode, string storeState, string storeCountry, string DoctorFirstName, string DoctorLastName, char Gender, DateTime DOB, string PhoneNumber, string UserName, string Password)
        {
            string companyRefCode = string.Empty;
            string resultCompanyRefCode = string.Empty;
            ZipcodeService zipService = new ZipcodeService();
            ZipCodeInfo zipinfo = new ZipCodeInfo();
            zipinfo = zipService.CalculateLatitudeAndLongitude(storePinCode);
            string latitude = zipinfo.Latitude;
            string longitude = zipinfo.Longitude;
            if (zipinfo.IsValidZipCode == true)
            {
                try
                {
                    CreateRegistrationInformationDataContext _context = new CreateRegistrationInformationDataContext();
                    var result = _context.CreateRegistrationInformation(companyRefCode, companyName, storeAddress1, storeAddress2, storeCity, storePinCode, storeState, storeCountry, DoctorFirstName, DoctorLastName, Gender, DOB, PhoneNumber, UserName, Password, latitude, longitude);

                    resultCompanyRefCode = result.FirstOrDefault().CompanyRefCode;
                    if (resultCompanyRefCode != "0")
                    {
                        return resultCompanyRefCode;
                    }
                    else if (resultCompanyRefCode == "1")
                    {
                        return "1"; //User name exits
                    }
                    else
                    {
                        return "0"; //Company Ref Code not available
                    }
                }
                catch (SqlException ex)
                {
                    return "-2"; //For duplicate username
                }
                catch (Exception ex)
                {
                    return ex.ToString(); //For Unsuccesful 
                }
            }
            else
            {
                return "Invalid ZipCode";
            }
               
        }

        //Existing companyRefCode
        public string Post(string companyRefCode, string DoctorFirstName, string DoctorLastName, char Gender, DateTime DOB, string PhoneNumber, string UserName, string Password, string storeAddress1, string storeAddress2, string storeCity, string storePinCode, string storeState,string storeCountry)
        {
            string userExist = 1.ToString();

            string companyName = string.Empty;
            string resultCompanyRefCode = string.Empty;
            ZipcodeService zipService = new ZipcodeService();
            ZipCodeInfo zipinfo = new ZipCodeInfo();
            
            zipinfo = zipService.CalculateLatitudeAndLongitude(storePinCode);
            if (zipinfo.IsValidZipCode == true)
            {
                try
                {
                    CreateRegistrationInformationDataContext _context = new CreateRegistrationInformationDataContext();
                    var result = _context.CreateRegistrationInformation(companyRefCode, companyName, storeAddress1, storeAddress2, storeCity, storePinCode, storeState, storeCountry, DoctorFirstName, DoctorLastName, Gender, DOB, PhoneNumber, UserName, Password, null, null);
                    resultCompanyRefCode = result.FirstOrDefault().CompanyRefCode;
                    if (resultCompanyRefCode != "0")
                    {
                        return resultCompanyRefCode;
                    }
                    else if (resultCompanyRefCode == "1")
                    {
                        return "1"; //User name exits
                    }
                    else
                    {
                        return "0"; //Company Ref Code not available
                    }
                }
                catch (SqlException ex)
                {
                    return "-2"; //For duplicate username
                }
                catch (Exception ex)
                {
                    return "-1"; //For Unsuccesful 
                }
            }
            else
            {
                return "Invalid ZipCode";
            }
           
        }
    }
}