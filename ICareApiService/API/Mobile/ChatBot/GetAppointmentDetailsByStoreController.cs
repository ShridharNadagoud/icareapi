﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetAppointmentDetailsByStoreController : ApiController
    {
        GetAppointmentDetailsByStoreDataContext _Context = new GetAppointmentDetailsByStoreDataContext();

        [Authorize]
        public List<sp_GetAppointmentDetailsByStoreResult> Get()
        {
            var appointmentList = _Context.sp_GetAppointmentDetailsByStore();
            return appointmentList.ToList();
        }
    }
}
