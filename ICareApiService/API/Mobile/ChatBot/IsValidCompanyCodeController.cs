﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class IsValidCompanyCodeController : ApiController
    {
        IsValidCompanyCodeDataContext _context = new IsValidCompanyCodeDataContext();
        public void Get(string companyRefCode)
        {
            Post(companyRefCode);
        }

        public string Post(string companyRefCode)
        {
            try
            {
                var result = _context.IsValidCompanyCode(companyRefCode);
                return result.FirstOrDefault().Column1;
            }
            catch
            {
                return string.Empty;
            }
            
        }
    }
}
