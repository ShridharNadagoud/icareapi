﻿
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Web;
using System.Web.Http;
using static Google.Apis.Drive.v3.DriveService;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetImagesFromDriveController : ApiController
    {
        static string[] Scopes = { DriveService.Scope.Drive };

        public string Get(string fileId)
        {
            string link;
            try
            {
                DriveService service = GetService();

               
               // string FolderPath = System.Web.HttpContext.Current.Server.MapPath("/GoogleDriveFiles/");
                FilesResource.GetRequest request = service.Files.Get(fileId);

                string FileName = request.Execute().Name;
                link = request.FileId;
                //string FilePath = System.IO.Path.Combine(FolderPath, FileName);

                MemoryStream stream1 = new MemoryStream();
                 
              

            }
            catch(Exception ex)
            {
                return ex.ToString();
            }

            return "https://drive.google.com/thumbnail?id="+link;
        }
        public static DriveService GetService()
        {
            UserCredential credential;
            string[] scopes = new string[] { DriveService.Scope.Drive }; // Full access

            
            using (var stream = new FileStream(HttpContext.Current.Server.MapPath("~/GoogleDriveCredentials/client_secret.json"), FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, HttpContext.Current.Server.MapPath("~/GoogleDriveCredentials/"));
                var ClientId = "98069384036-6q08kc8oekpc111q4nte8mj1hvvaci6d.apps.googleusercontent.com";
                var ClientSecret = "Px5WUJR6DIffeEC7wjBmz01p";
                var clientSecrets = new ClientSecrets() { ClientId = ClientId, ClientSecret = ClientSecret };
                credential = GoogleWebAuthorizationBroker
                               .AuthorizeAsync(
                    clientSecrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(HttpContext.Current.Server.MapPath("~/GoogleDriveCredentials/"), true)
                    ).Result;
            }

             
            var services = new DriveService(new BaseClientService.Initializer()
            {
                //ApiKey = "AIzaSyCLN7bo4NHpEQZ04IhJuS1C5v5rIjv5JhE",  // from https://console.developers.google.com (Public API access)
                HttpClientInitializer = credential,

                ApplicationName = "ICareApiService",
            });
             
            return services;
        }

    }


}

