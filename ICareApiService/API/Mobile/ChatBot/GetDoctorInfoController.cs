﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetDoctorInfoController : ApiController
    {
        GetDoctorInfoDataContext _context = new GetDoctorInfoDataContext();
        public List<GetDoctorInfoResult> Get(string companyRefCode, string userName, string password)
        {
            var _DoctorInfo = _context.GetDoctorInfo(companyRefCode, 0, userName, password);
            return _DoctorInfo.ToList();
        }
    }
}