﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using ICareApiService.Models;
using ICareApiService.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class UpdateDoctorDetailsController : ApiController
    {
        public int Get(int DoctorId,int CompnayId,string CompanyRefCode,string CompanyName, string PhoneNumber, string Address, string City, string State, string Zipcode)
        {
            int result = Post(DoctorId, CompnayId, CompanyRefCode, CompanyName, PhoneNumber, Address, City, State, Zipcode);
            return 1;
        }

        private int Post(int doctorId,int companyId,string companyRefCode,string companyName, string phoneNumber, string address, string city, string state, string zipcode)
        {
            ZipcodeService zipService = new ZipcodeService();
            ZipCodeInfo zipinfo = new ZipCodeInfo();
            zipinfo = zipService.CalculateLatitudeAndLongitude(zipcode);
            try
            {
                UpdateDoctorDetailsDataContext _context = new UpdateDoctorDetailsDataContext();
                var result = _context.UpdateDoctorDetails(doctorId,companyId,companyRefCode,companyName, phoneNumber, address, city, state, zipcode, zipinfo.Latitude, zipinfo.Longitude);
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}