﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SendForgotPasswordEmailController : ApiController
    {
        string _password;

        public string Get(string companyRefCode, string userName)
        {
            GetDoctorPasswordDataContext _context = new GetDoctorPasswordDataContext();            
            try
            {
                var password = _context.GetDoctorPassword(userName);
                _password = password.FirstOrDefault().Password;
                if (_password == "0")
                {
                    return "0"; //User does not exist
                }
                else
                {
                    return SendEmail(userName, _password);
                }
            }
            catch(Exception ex)
            {
                return "0";
            }
        }

      
        public string Get(string userName)
        {
            GetCustomerPasswordDataContext _contextP = new GetCustomerPasswordDataContext();
            string _password;
            var password = _contextP.GetCustomerPassword(userName);
            _password = password.FirstOrDefault().Password;
            try
            {
                if (_password == "0")
                {
                    return "0"; //User does not exist
                }
                else
                {
                    return SendEmail(userName, _password);
                }
            }
            catch(SqlException ex)
            {
                return "0";//User does not exist
            }
        }


        public string SendEmail(string userName, string password)
        { 
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials =
            new System.Net.NetworkCredential("optirisesoftwares@gmail.com", "UsiIcare@54321");
            SmtpServer.EnableSsl = true;
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("optirisesoftwares@gmail.com");
                mail.To.Add(userName);
                mail.Subject = "iCareBuddy Password Assistance";
                string htmlBody= "<p align=center>We received a request to send password associated with this e-mail address. If you made this request, please find your Password below.</p>";
                htmlBody += "<h2  align=center><font color=DIMGRAY>" + password + "</font></h2>";
                htmlBody += "<p align=center>If you did not request to send your password  you can safely ignore this email. Rest assured your customer account is safe.</p>";

                mail.Body = htmlBody;
                mail.IsBodyHtml = true;

                SmtpServer.Send(mail);
                return "1"; //Successfully send
            }
            catch
            {
                return "0"; //Email unsuccessful
            }
        }        
    }
}
               