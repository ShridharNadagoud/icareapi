﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class CheckDoctorAvailableSlotsController : ApiController
    {
        CheckDoctorAvailableSlotsDataContext context = new CheckDoctorAvailableSlotsDataContext();
        public string Get(int docId,DateTime appDate,string appTime)
        {
            try
            {
                var result = context.CheckDoctorAvailableSlots(docId, appDate, appTime);
                var finalResult = result.FirstOrDefault();
                if (finalResult == null)
                {
                    return "0";
                }
                else
                {
                    return "1";
                }
            }
            catch (Exception)
            {
                return "2";
            }
        }
    }
}
