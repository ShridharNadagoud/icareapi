﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetDoctorDisplayDPController : ApiController
    {
        GetDoctorDisplayDPDataContext context = new GetDoctorDisplayDPDataContext();
        public string Get(int doctorId,int companyId,string companyRefCode)
        {
            var data = context.GetDoctorDisplayDP(doctorId, companyId, companyRefCode);
            return data.FirstOrDefault().FilePath;
        }
    }
}
