﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SaveCustomerExamController : ApiController
    {

        // GET: api/SavePatientExam/5

        public void Get(int patientId, int doctorId, DateTime examDate, DateTime expireDate, string remarks, string attributedetails, int productId, int customerDocumentId)
        {

            Post(patientId, doctorId, examDate, expireDate, remarks, attributedetails, productId, customerDocumentId);
        }
        
        public string Post(int patientId, int doctorId, DateTime examDate, DateTime expireDate, string remarks, string attributedetails, int productId, int customerDocumentId)
        {
            string msg = string.Empty;
            try
            {
                SaveCustomerExamDataContext _Context = new SaveCustomerExamDataContext();
                _Context.SavePatientExam(patientId, doctorId, examDate, expireDate, remarks, attributedetails, productId, customerDocumentId);
                _Context.SubmitChanges();
                msg = "Saved !";
            }
            catch
            {
                msg = "Falied!";
            }
            return msg;
        }
    }
}