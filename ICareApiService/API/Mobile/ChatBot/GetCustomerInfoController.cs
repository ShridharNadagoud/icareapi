﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetCustomerInfoController : ApiController
    {
        GetCustomerInfoDataContext _context = new GetCustomerInfoDataContext();

        [Authorize]
        public List<GetCustomerInfoResult> Get(string userName, string password)
        {
            var _CustomerInfo = _context.GetCustomerInfo( userName, password);
            return _CustomerInfo.ToList();
        }
    }
}
