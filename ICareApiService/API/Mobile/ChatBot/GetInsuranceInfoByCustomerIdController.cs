﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetInsuranceInfoByCustomerIdController : ApiController
    {
        GetInsuranceIfoByCustomerIdDataContext context = new GetInsuranceIfoByCustomerIdDataContext();
        public List<GetInsuranceIfoByCustomerIdResult> Get(int CustomerId)
        {
                var result = context.GetInsuranceIfoByCustomerId(CustomerId);
                return result.ToList();
        }
    }
}
