﻿using ICareApiService.LinqToSql;
using ICareApiService.LinqToSql.Mobile.ChatBot;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
namespace ICareApiService.API.Mobile.ChatBot
{
    public class ExamEmailNotificationController : ApiController
    {
        byte[] bytes;
        DateTime ExpireDate;
        string ExpireDateString;
        Uri imageLink;
        string url;
        public bool Get(string toAddress, string docName, string companyName, string customerName, string dPhoneNum, string examDate, string ExamType, string pPhnNum, string rSph, string rCyl, string rAxis, string rVa, string rAdd, string rFpd, string rNpd, string lSph, string lCyl, string lAxis, string lVa, string lAdd, string lFpd, string lNpd,string EmailId )
        {
            return Post(toAddress,docName,companyName,customerName,dPhoneNum,examDate, ExamType, pPhnNum,rSph,rCyl,rAxis,rVa,rAdd,rFpd, rNpd, lSph,lCyl,lAxis, lVa, lAdd, lFpd,lNpd, EmailId);
        }
        public bool Post(string toAddress, string docName, string companyName, string customerName, string dPhoneNum, string examDate, string ExamType, string pPhnNum, string rSph, string rCyl, string rAxis, string rVa, string rAdd, string rFpd, string rNpd, string lSph, string lCyl, string lAxis, string lVa, string lAdd, string lFpd, string lNpd ,string EmailId)
        {
             ExpireDate = Convert.ToDateTime(examDate);
            ExpireDate = ExpireDate.AddYears(1);
            ExpireDateString = ExpireDate.ToString("MMM dd,yyyy");
            DataTable data_table = new DataTable();
            GetCompanyLogoDataContext _context = new GetCompanyLogoDataContext();
            //GetDoctorDisplayImageDataContext _context = new GetDoctorDisplayImageDataContext();
            var data = _context.GetCompanyLogo(485);
            url = data.FirstOrDefault().FilePath;
            if (string.IsNullOrEmpty(url))
            {
                url = "/assets/files/saverximage/IMG_20180608_123450.jpg";
            }
             imageLink = new Uri("http://icareapi.azurewebsites.net" + url);
            string ImgSource = imageLink.ToString();
            string CustomerName = customerName.First().ToString().ToUpper() + customerName.Substring(1);
            string DoctName= docName.First().ToString().ToUpper() + docName.Substring(1);
            //Mail Body
            string testbody = @"<html>
<head>";
            testbody += @"<p> Dear " + CustomerName + ",</p>";
            testbody += @"<p margin-left:10%;>Thank you for visiting  <font style =""font-size:12px; font-weight:bold;"" >Dr." + docName + "</font> On <u>" + examDate + "</u>.</font></p>";
            testbody += "<p margin-left:10%;> Please find yours Rx details below.</p><br/>";
            testbody +=@"<table align=""center"" style=""border: 1px solid"">
                 <tbody>
                     <tr>
                         <td>";

            testbody += @"<img  style =""float:right"" height =""50"" src=" + ImgSource + "  width =" + "70" + " class="+"CToWUd"+" >";

            testbody += @"</td>
						<td>";
            testbody += @"<p style =""float:left;font-size:25px;text-align:center;font-weight:bold;"" ><font color=""#090972"">" + companyName + "</font></p>";
            testbody += @"</td>
					</tr>
                    <tr>
                        <td colspan =""2"">
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=""2"">
							<table border =""0"" >
                                <tbody>
                                    <tr>
                                        <td>
                                            <table align=""center"">
												<tbody>
													<tr>";
            testbody += @"<td>
															Doctor Name :</td>
														<td>
															<font color = ""Black"" >" + docName + "</font></td>";

            testbody += @"<td>
															Phone No:</td>
														<td>
															<font color = ""DodgerBlue"" ><u>" + dPhoneNum + " </u></ font ></ td >";

            testbody += @"</tr>

                                                     <tr>

                                                        <td>
                                                            Patient Name :</td>
														<td>";
            testbody += @"<font color = ""Black"" >" + customerName + "</font></td>";
            testbody += @"<td>
															Phone No:</td>
														<td>
															<font color = ""DodgerBlue"" ><u>" + pPhnNum + " </u></font></td";
            testbody += @"</tr>
                                                    <tr>
                                                        <td>
                                                            Exam Type :</td>
														<td>
															<font color = ""Black"" > " + ExamType + "</font></td>";

            testbody += @"<td>
															&nbsp;</td>
														<td>
															&nbsp;</td>
													</tr>
													<tr>
														<td>
															Exam Date :</td>
														<td>
															<font color = ""Black"" > May 31, 2018</font></td>
														<td>
															&nbsp;</td>
														<td>
															&nbsp;</td>
													</tr>
												</tbody>
											</table>
											 <br>
											<table align = ""center"" border=""1"" style=""width:100%;margin-top:5%;margin-bottom:10%"">
												<tbody>
													<tr>
														<th width=""100"">
															<font color = ""red""> Rx </font></th>
                                        <th bgcolor=""#CCCCCF"" width=""60"">
                                                          SPH </th>
                                                        <th bgcolor=""#F4D03F"" width=""60"">
                                                            CYL </th>
                                                        <th bgcolor=""#EDBB99"" width=""60"">
                                                            AXIS </th>
                                                        <th bgcolor=""#A3E4D7"" width=""60"">
                                                            ADD </th>
                                                        <th bgcolor=""#85C1E9"" width=""60"">
                                                            PD </th>
                                                    </tr>
                                                    <tr>";
            testbody += @"<td>
                                                            O.D.(Right Eye) </td>
                                                        <td align=""center"">
															" + rSph + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + rCyl + "</td>";
            testbody += @"<td align=""center"">
															" + rAxis + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + rAdd + " </td>";
            testbody += @"<td align=""center"">
															" + rFpd + "</td>";
            testbody += @"</tr>";
            testbody += @"<tr><td>
															O.S.(Left Eye)</td>";
            testbody += @"<td align = ""center"" >
                                                            " + lSph + "</td>";
            testbody += @"<td align=""center"">
															" + lCyl + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + lAxis + "</td>";
            testbody += @"<td align=""center"">
															" + lAdd + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + lFpd + " </td >";
            testbody += @"</tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                 <td style=""background-color:DodgerBlue;color:white;padding:8px"">";
        //    testbody +=@"<font>"+address1+",</font><br>";
        //    testbody += @"<font>" + address2 + ",</font><br>"; 
        //    testbody += @"<font>" + city + "," + state + "-" + zipcode + "</font><br>";
								//testbody +=@"</td>
						testbody += @"</td></tr>
								</tbody>
							</table>
						</td>
					</tr>
                </tbody>
			</table>";
   testbody += @"<h2><font color = 'DodgerBlue' > Thanks.</font></h2><br> ";
            testbody += "<font >&nbsp;&nbsp;&nbsp;" + EmailId + "</font>";
            testbody += "<h2><font color='DodgerBlue'>" + companyName + ".</font></h2>";
 testbody +=@"</html>
";
              
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials =
            new System.Net.NetworkCredential("optirisesoftwares@gmail.com", "UsiIcare@54321");
            SmtpServer.EnableSsl = true;
            //SmtpServer.UseDefaultCredentials = false;

            try
            {
                MailMessage mail = new MailMessage();
                //Conver HTML to PDF and Attached to mail

                StringBuilder sb = new StringBuilder();
                sb.Append("<header class='clearfix'>");

                sb.Append("<br><br>");
                sb.Append("<div style=width:100 %;align=center>");
                sb.Append("<div align=center style=border:1px solid; width:50%;>");
                sb.Append("<img src=" + imageLink + " style=margin-right:70px; align=center id='img' alt='' width='100px' height='70px'/>");
                sb.Append("<br>");

                sb.Append("<p style=font-size:30px; text-align:center;font-weight:bold; margin-top:0px; margin-bottom:0px><font color='#090972'>" + companyName + " </font></p>");
                sb.Append("<div>");
                sb.Append("<br><br><br>");
                sb.Append("<p style=font-size:22px; text-align:left;margin-left:22px;>");
                sb.Append(" Dr."+ DoctName);
                sb.Append("</p>");
                sb.Append("<br>");
                //sb.Append("<p style=font-size:15px; text-align:left;margin-left:22px;>");
                //sb.Append(" Address of");
                //sb.Append("</p>");
                sb.Append("<p style=font-size:15px; text-align:left;margin-left:22px;>");
                sb.Append("Contact Number&nbsp;:"+ dPhoneNum+" </p>");
                sb.Append("<br><br><br><br>");
                sb.Append("</div></br></br>");
                sb.Append("</div>");

                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Patient Name &nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +customerName);
                sb.Append("</h4>");
                sb.Append("<br>");
                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Exam Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+ExamType);
                sb.Append("</h4>");
                sb.Append("<br>");
                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Exam Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+examDate);
                sb.Append("</h4>");
                sb.Append("<br>");
                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Expiration date &nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+ ExpireDateString);
                sb.Append("</h4>");
                sb.Append("<br><br><br>");
                sb.Append("<div style=width:100 %;align=center>");

                sb.Append("<table border='1' style=width:28%;margin-top:5%;margin-left:50px;margin-right:50px; margin-bottom:10% align=center>");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<th>");
                sb.Append("<font color='red'>Rx</font></th>");
                sb.Append("<th align='center'>SPH</th>");
                sb.Append("<th align='center' >CYL</th>");
                sb.Append("<th align='center'>AXIS</th>");
                sb.Append("<th align='center'>ADD</th>");
                sb.Append("<th align='center'>PD</th>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>OD</td>");
                sb.Append("<td align='center'>"+ rSph+"</td>");
                sb.Append("<td align='center'>"+rCyl+"</td>");
                sb.Append("<td align='center'>"+rAxis+"</td>");
                sb.Append("<td align='center'>"+rAdd+"</td>");
                sb.Append("<td align='center'>"+ rFpd + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr><td>OS</td><td align='center'>"+lSph+"</td><td align='center'>"+ lCyl+"</td><td align='center'>"+lAxis+"</td><td align='center'>"+lAdd+"</td>");
                sb.Append("<td align='center'>"+lFpd+"</td></tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("<br><br><br><br><br><br><br>");

                sb.Append("<h3 style=text-align:Right;font-size:20px; margin-Right:10px;>");
                sb.Append("Prescribed by: ___________________________ </h3>");
                sb.Append("</div>");


                sb.Append("</footer>");
                StringReader sr = new StringReader(sb.ToString());

                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    pdfDoc.Open();

                    htmlparser.Parse(sr);
                    pdfDoc.Close();

                    bytes = memoryStream.ToArray();
                    memoryStream.Close();
                }
                mail.From = new MailAddress("optirisesoftwares@gmail.com");
                mail.To.Add(toAddress);
                mail.Bcc.Add(EmailId);
                mail.Subject = "Prescription";
                mail.Body = testbody;
                mail.Attachments.Add(new Attachment(new MemoryStream(bytes), "Prescription.pdf"));
                mail.IsBodyHtml = true;
                //mail.Body = "Hi  \r\n Doctore Name="+DocName+" \r\n Doctor EmailID="+UserName+ " \r\n Company Name=" + CompanyName+ " \r\n PhoneNumber=" +PhNumber+ "\r\n \r\n \r\n \r\n \r\n Exam Successfully Created";
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
    }


