﻿using ICareApiService.LinqToSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class GetCustomerIdByCustomerGuIdController : ApiController
    {
        GetCustomerIdByCustomerGuIdDataContext contest = new GetCustomerIdByCustomerGuIdDataContext();

        public string Get(string userName)
        {
            try
            {
                var customerId = contest.GetCustomerIdByCustomerGuId(userName);
                return customerId.FirstOrDefault().customerId.ToString();
            }catch(Exception ex)
            {
                return "1";
            }
        }
    }
}
