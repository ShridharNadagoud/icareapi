﻿using ICareApiService.LinqToSql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SendExamNotificationToStoreController : ApiController
    {
        public bool Get(string toAddress, string docName, string companyName, string customerName, string dPhoneNum, string examDate, string ExamType, string pPhnNum, string rSph, string rCyl, string rAxis, string rVa, string rAdd, string rFpd, string rNpd, string lSph, string lCyl, string lAxis, string lVa, string lAdd, string lFpd, string lNpd, string EmailId)
        {
            return Post(toAddress, docName, companyName, customerName, dPhoneNum, examDate, ExamType, pPhnNum, rSph, rCyl, rAxis, rVa, rAdd, rFpd, rNpd, lSph, lCyl, lAxis, lVa, lAdd, lFpd, lNpd, EmailId);
        }
        public bool Post(string toAddress, string docName, string companyName, string customerName, string dPhoneNum, string examDate, string ExamType, string pPhnNum, string rSph, string rCyl, string rAxis, string rVa, string rAdd, string rFpd, string rNpd, string lSph, string lCyl, string lAxis, string lVa, string lAdd, string lFpd, string lNpd, string EmailId )
        {
            DataTable data_table = new DataTable();
            GetCompanyLogoDataContext _context = new GetCompanyLogoDataContext();
            //GetDoctorDisplayImageDataContext _context = new GetDoctorDisplayImageDataContext();
            var data = _context.GetCompanyLogo(449);
            string url = data.FirstOrDefault().FilePath;
            Uri imageLink = new Uri("http://icareapi.azurewebsites.net" + url);
            string ImgSource = imageLink.ToString();
            string CustomerName = customerName.First().ToString().ToUpper() + customerName.Substring(1);
            string DoctName = docName.First().ToString().ToUpper() + docName.Substring(1);
            string testbody = @"<html>
<head>";
            testbody += @"<p> Dear " + CustomerName + ",</p>";
            testbody += @"<p margin-left:10%;>Thank you for visiting  <font style =""font-size:12px; font-weight:bold;"" >Dr." + docName + "</font> On <u>" + examDate + "</u>.</font></p>";
            testbody += "<p margin-left:10%;> Please find yours Rx details below.</p><br/>";
            testbody += @"<table align=""center"" style=""border: 1px solid"">
                 <tbody>
                     <tr>
                         <td>";

            testbody += @"<img  style =""float:right"" height =""50"" src=" + ImgSource + "  width =" + "70" + " class=" + "CToWUd" + " >";

            testbody += @"</td>
						<td>";
            testbody += @"<p style =""float:left;font-size:25px;text-align:center;font-weight:bold;"" ><font color=""#090972"">" + companyName + "</font></p>";
            testbody += @"</td>
					</tr>
                    <tr>
                        <td colspan =""2"">
                            <hr/>
                        </td>

                    </tr>
                    <tr>
                        <td colspan=""2"">
							<table border =""0"" >
                                <tbody>
                                    <tr>
                                        <td>
                                            <table align=""center"">
												<tbody>
													<tr>";
            testbody += @"<td>
															Doctor Name :</td>
														<td>
															<font color = ""Black"" >" + docName + "</font></td>";

            testbody += @"<td>
															Phone No:</td>
														<td>
															<font color = ""DodgerBlue"" ><u>" + dPhoneNum + " </u></ font ></ td >";

            testbody += @"</tr>

                                                     <tr>

                                                        <td>
                                                            Patient Name :</td>
														<td>";
            testbody += @"<font color = ""Black"" >" + customerName + "</font></td>";
            testbody += @"<td>
															Phone No:</td>
														<td>
															<font color = ""DodgerBlue"" ><u>" + pPhnNum + " </u></font></td";
            testbody += @"</tr>
                                                    <tr>
                                                        <td>
                                                            Exam Type :</td>
														<td>
															<font color = ""Black"" > " + ExamType + "</font></td>";

            testbody += @"<td>
															&nbsp;</td>
														<td>
															&nbsp;</td>
													</tr>
													<tr>
														<td>
															Exam Date :</td>
														<td>
															<font color = ""Black"" > May 31, 2018</font></td>
														<td>
															&nbsp;</td>
														<td>
															&nbsp;</td>
													</tr>
												</tbody>
											</table>
											 </br>
											<table align = ""center"" border=""1"" style=""width:100%;margin-top:5%;margin-bottom:10%"">
												<tbody>
													<tr>
														<th width=""100"">
															<font color = ""red""> Rx </font></th>

                                        <th bgcolor=""#CCCCCF"" width=""60"">
                                                          SPH </th>
                                                        <th bgcolor=""#F4D03F"" width=""60"">
                                                            CYL </th>

                                                        <th bgcolor=""#EDBB99"" width=""60"">
                                                            AXIS </th>

                                                        <th bgcolor=""#A3E4D7"" width=""60"">
                                                            ADD </th>

                                                        <th bgcolor=""#85C1E9"" width=""60"">
                                                            PD </th>

                                                    </tr>

                                                    <tr>";

            testbody += @"<td>
                                                            O.D.(Right Eye) </td>

                                                        <td align=""center"">
															" + rSph + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + rCyl + "</td>";
            testbody += @"<td align=""center"">
															" + rAxis + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + rAdd + " </td>";


            testbody += @"<td align=""center"">
															" + rFpd + "</td>";
            testbody += @"</tr>";
            testbody += @"<tr><td>
															O.S.(Left Eye)</td>";
            testbody += @"<td align = ""center"" >
                                                            " + lSph + "</td>";
            testbody += @"<td align=""center"">
															" + lCyl + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + lAxis + "</td>";
            testbody += @"<td align=""center"">
															" + lAdd + "</td>";
            testbody += @"<td align = ""center"" >
                                                            " + lFpd + " </td >";
            testbody += @"</tr>

                                                </tbody>

                                            </table>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td style=""background-color:DodgerBlue;color:white;padding:8px"">";
            //testbody += @"<font>" + address1 + ",</font><br>";
            //testbody += @"<font>" + address2 + ",</font><br>";
            //testbody += @"<font>" + city + "," + state + "-" + zipcode + "</font><br>";

                               testbody +=@"</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
         

                </tbody>
			</table>";
            testbody += @"<h2><font color = 'DodgerBlue' > Thanks.</font></h2><br> ";
            testbody += "<font >&nbsp;&nbsp;&nbsp;" + EmailId + "</font>";
            testbody += "<h2><font color='DodgerBlue'>" + companyName + ".</font></h2>";
            testbody += @"</html>";
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials =
            new System.Net.NetworkCredential("optirisesoftwares@gmail.com", "UsiIcare@54321");
            SmtpServer.EnableSsl = true;
            //SmtpServer.UseDefaultCredentials = false;
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("optirisesoftwares@gmail.com");
                mail.To.Add(toAddress);
                mail.Subject = "Prescription";
                mail.Body = testbody;
                mail.IsBodyHtml = true;
                //mail.Body = "Hi  \r\n Doctore Name="+DocName+" \r\n Doctor EmailID="+UserName+ " \r\n Company Name=" + CompanyName+ " \r\n PhoneNumber=" +PhNumber+ "\r\n \r\n \r\n \r\n \r\n Exam Successfully Created";
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
