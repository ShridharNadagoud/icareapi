﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class SendEmailForgotPasswordController : ApiController
    {
        byte[] bytes;
        public bool Get()
        {
            return Post();
        }
        public bool Post()
        {
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials =
            new System.Net.NetworkCredential("optirisesoftwares@gmail.com", "UsiIcare@54321");
            SmtpServer.EnableSsl = true;
            try
            {
                GetDoctorDisplayImageDataContext _context = new GetDoctorDisplayImageDataContext();

                var data = _context.GetDoctorDisplayImage(307);
                string url= data.FirstOrDefault().FilePath;
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("kiranhoney11@gmail.com");
                mail.To.Add("kiran.nelvai11@gmail.com");
                mail.Subject = "Kiran testing";

                StringBuilder sb = new StringBuilder();
                sb.Append("<header class='clearfix'>");
                
                sb.Append("<br><br>");
                Uri link=new Uri("http://icareapi.azurewebsites.net"+url);
                sb.Append("<div style=width:100 %;align=center>");
                sb.Append("<div align=center style=border:1px solid; width:50%;>");
                sb.Append("<img src=" + link + " style=margin-right:70px; align=center id='img' alt='' width='100px' height='70px'/>");
                sb.Append("<br>");
                sb.Append("<p style=font-size:30px; text-align:center;font-weight:bold; margin-top:0px; margin-bottom:0px><font color='DodgerBlue'> Vision I Plus </font></p>");
                sb.Append("<div>");
                sb.Append("<br><br><br>");
                sb.Append("<p style=font-size:22px; text-align:left;margin-left:22px;>");
                sb.Append(" Dr.Kiran");
                sb.Append("</p>");
                sb.Append("<br>");
                sb.Append("<p style=font-size:15px; text-align:left;margin-left:22px;>");
                sb.Append(" Address of Doctor Store");
                sb.Append("</p>");
                sb.Append("<p style=font-size:15px; text-align:left;margin-left:22px;>");
                sb.Append("Contact Number&nbsp;: 9177414341</p>");
                sb.Append("<br><br><br><br>");
                sb.Append("</div></br></br>");
                sb.Append("</div>");

                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Patient Name &nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Srujan Kumar");
                sb.Append("</h4>");
                sb.Append("<br>");
                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Exam Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regular Eye Checkup");
                sb.Append("</h4>");
                sb.Append("<br>");
                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Exam Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;20/05/2019");
                sb.Append("</h4>");
                sb.Append("<br>");
                sb.Append("<h4 style=font-size:18px; text-align:left;margin-left:20px;>");
                sb.Append("Expiration date &nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;20/05/2019");
                sb.Append("</h4>");
                sb.Append("<br><br><br>");
                sb.Append("<div style=width:100 %;align=center>");

                sb.Append("<table border='1' style=width:28%;margin-top:5%;margin-left:50px;margin-right:50px; margin-bottom:10% align=center>");
                sb.Append("<tbody>");
                sb.Append("<tr>");
                sb.Append("<th>");
                sb.Append("<font color='red'>Rx</font></th>");
                sb.Append("<th align='center'>SPH</th>");
                sb.Append("<th align='center' >CYL</th>");
                sb.Append("<th align='center'>AXIS</th>");
                sb.Append("<th align='center'>ADD</th>");
                sb.Append("<th align='center'>PD</th>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td>OD</td>");
                sb.Append("<td align='center'> 0.75</td>");
                sb.Append("<td align='center'> 0.75</td>");
                sb.Append("<td align='center'> 0.75</td>");
                sb.Append("<td align='center'> 0.75</td>");
                sb.Append("<td align='center'> 0.75</td>");
                sb.Append("</tr>");
                sb.Append("<tr><td>OS</td><td align='center'>0.5</td><td align='center'>-0.25</td><td align='center'>5</td><td align='center'></td>");
                sb.Append("<td align='center'></td><td align='center'></td><td align='center'></td></tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("<br><br><br><br><br><br><br>");

                sb.Append("<h3 style=text-align:Right;font-size:20px; margin-Right:10px;>");
                sb.Append("Prescribed by: ___________________________ </h3>");
                sb.Append("</div>");


                sb.Append("</footer>");
                StringReader sr = new StringReader(sb.ToString());

                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    pdfDoc.Open();

                    htmlparser.Parse(sr);
                    pdfDoc.Close();

                    bytes = memoryStream.ToArray();
                    memoryStream.Close();
                }
                mail.Attachments.Add(new Attachment(new MemoryStream(bytes), "Prescription.pdf"));
                //string sampleHtml = "<html><body><p>Simple HTML string</p></body></html> ";
                //System.Web.Configuration.Converter.ConvertHtmlString(sampleHtml, @"C:\\Document.pdf");
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
