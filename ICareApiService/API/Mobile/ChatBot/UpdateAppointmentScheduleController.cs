﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile.ChatBot
{
    public class UpdateAppointmentScheduleController : ApiController
    {
        UpdateAppointmentScheduleDataContext _dataContext = new UpdateAppointmentScheduleDataContext();
        public int Get(int customerId, DateTime previousDateOfAppointment, DateTime updatedDateOfAppointment, string previousStartTime, string updatedStartTime, string updatedEndTime, int updatedReasonForVisitId, string updatedAppointmentNotes)
        {
            return Post(customerId, previousDateOfAppointment, updatedDateOfAppointment, previousStartTime, updatedStartTime, updatedEndTime, updatedReasonForVisitId, updatedAppointmentNotes);
        }

        public int Post(int customerId, DateTime previousDateOfAppointment, DateTime updatedDateOfAppointment, string previousStartTime, string updatedStartTime, string updatedEndTime, int updatedReasonForVisitId, string updatedAppointmentNotes)
        {
            try
            {
                var result = _dataContext.UpdateAppointmentSchedule(customerId, previousDateOfAppointment, updatedDateOfAppointment, previousStartTime, updatedStartTime, updatedEndTime, updatedReasonForVisitId, updatedAppointmentNotes);                
                return result; //returning 0 that is updated successfully
            }
            catch
            {
                return 1; //Not updated successfully
            }
        }
    }
}
