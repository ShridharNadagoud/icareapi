﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web.Http;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Threading;
using System.Web;
using File = Google.Apis.Drive.v2.Data.File;
using IFile = Google.Apis.Drive.v3.Data.File;
using System.Web.Http;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure;

namespace ICareApiService.API.Mobile
{
    public class SaveRxImageAzureNewController : ApiController
    {
        const string storageAccountName = "patientdocument";
        const string StorageAccountKey = "suNg+b6+XXfk8ayQEvqV0MK+H9o60BaS0b+c39MXGoLoPLcc2OIyh7Wt2ca5+vLXxkTOypeySUQz0s71blspww==";

        System.Drawing.Image localimage;
        private static Random random = new Random();
        byte[] document = null;
        string fileId;
        string singlefilePathName = string.Empty;
        string returnId;
        string AzurePath;
        string nameOfImage;
        string CurrentDate;
        static string[] scopes = { DriveService.Scope.Drive };
        SaveRxImageDataContext _Context = new SaveRxImageDataContext();
        public string Post(string companyRefCode, string customerId ,int doctorId)
        {

            

            try
            {

                // var myUniqueFileName = $@"{Guid.NewGuid()}.txt";
                //var myUniqueFileName1 = $@"{DateTime.Now.Ticks}.txt";
                CurrentDate= DateTime.Now.ToString("MMM d,yyyy");
                var httpRequest = HttpContext.Current.Request;
                string fileName = string.Empty, fileContentType = string.Empty;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        fileName = postedFile.FileName;
                        if (fileName.Contains("\\") || fileName.Contains(@"\"))
                        {
                            fileName = postedFile.FileName.Split('\\').LastOrDefault().Split('/').LastOrDefault();
                        }
                        var data = fileName.Split('.');//postedFile.ContentType;
                        fileContentType = data.LastOrDefault();
                        Stream str = postedFile.InputStream;
                        BinaryReader Br = new BinaryReader(str);
                        // document = Br.ReadBytes((Int32)str.Length);
                        document = null;
                        localimage = System.Drawing.Image.FromStream(str);
                        string folderName = companyRefCode;

                        //AzurePath = Path.Combine(HttpContext.Current.Server.MapPath("~/GoogleDrive/"));

                        //if (!Directory.Exists(AzurePath))  // if it doesn't exist, create
                        //    Directory.CreateDirectory(AzurePath);
                        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        var imageName = new string(Enumerable.Repeat(chars, 5)
                          .Select(s => s[random.Next(s.Length)]).ToArray());
                        nameOfImage = imageName + "." + fileContentType;
                        AzurePath = Path.Combine(HttpContext.Current.Server.MapPath("~/GoogleDrive/"+ postedFile.FileName));
                        //googlepath = Path.Combine(HttpContext.Current.Server.MapPath("~/GoogleDrive/" + postedFile.FileName));
                        string resized = ResizeImage(str, new Size(3264, 2440), AzurePath);
                        try

                        {
                            Microsoft.WindowsAzure.Storage.Auth.StorageCredentials creden = new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageAccountName, StorageAccountKey);

                            Microsoft.WindowsAzure.Storage.CloudStorageAccount acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(creden, useHttps: true);

                            CloudBlobClient client = acc.CreateCloudBlobClient();

                            CloudBlobContainer cont = client.GetContainerReference("patientdocument");

                            cont.CreateIfNotExists();

                            cont.SetPermissions(new BlobContainerPermissions
                            {

                                PublicAccess = BlobContainerPublicAccessType.Blob
                            });
                            companyRefCode= companyRefCode.First().ToString().ToUpper() + companyRefCode.Substring(1);
                            CloudBlockBlob cblob = cont.GetBlockBlobReference(companyRefCode + "/"+ doctorId + "/"+ customerId + "/"+ CurrentDate+"/"+ postedFile.FileName);

                            using (var imagestream = new System.IO.FileStream(AzurePath, System.IO.FileMode.Open))
                            {

                                cblob.UploadFromStream(imagestream);
                            }
                            var filename = cblob.Uri;
                            fileId = filename.ToString();
                        }
                        catch (Exception ex)
                        {
                            return ex.ToString();
                        }

                        var result = _Context.SaveRxImage(document, postedFile.FileName, fileContentType, fileId);
                        //Delete Temp Images
                        var deletePath = HttpContext.Current.Server.MapPath("~/GoogleDrive/");
                        string[] filePaths = Directory.GetFiles(deletePath);
                        foreach (string filePath in filePaths)
                            System.IO.File.Delete(filePath);

                        returnId = result.FirstOrDefault().CustomerDocumentId.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return fileId+ "|" + returnId;
        }

        //Compress Image

        private string ResizeImage(Stream localimage, Size size, string targetPath)
        {

            int originalWidth = size.Width;
            int originalHeight = size.Height;
            float percentWidth = (float)size.Width / (float)originalWidth;
            float percentHeight = (float)size.Height / (float)originalHeight;
            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;

            // Image newImage = new Bitmap(newWidth, newHeight);
            using (var image = System.Drawing.Image.FromStream(localimage))
            {

                // var newWidth = (int)(originalWidth *);
                //var newHeight = (int)(originalHeight * );

                 var newWidth = (int)(image.Width * 0.5);
                 var newHeight = (int)(image.Height*0.6);
                //var newWidth = 700;
                //var newHeight = 600;
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.PixelOffsetMode = PixelOffsetMode.HighQuality;
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                //thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
            return targetPath;
        }

    }
}
