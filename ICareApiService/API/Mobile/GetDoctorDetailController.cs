﻿using ICareApiService.LinqToSql.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile
{
    public class GetDoctorDetailController : ApiController
    {
        // GET: GetDoctorDetail
        [Authorize]
        public List<SP_GetDoctorDetailResult> Get(int employeeId, string userName, string password)
        {
            AuthenticateDoctorDataContext _AuthenticateContext = new AuthenticateDoctorDataContext();
            GetDoctorInfoDataContext _DoctorInfoContext = new GetDoctorInfoDataContext();

            var DoctorEmpId = _AuthenticateContext.AuthenticateDoctor(employeeId, userName, password);

            if(DoctorEmpId != null)
            {
               var  _DoctorInfo = _DoctorInfoContext.SP_GetDoctorDetail(DoctorEmpId.FirstOrDefault().EmployeeId);
               return _DoctorInfo.ToList();
            }
            else
            {
                return null;
            }
        }
    }
}