﻿using ICareApiService.LinqToSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API.Mobile
{
    public class GetDoctorAvailabiltyByStoreByDateByDoctorController : ApiController
    {
        SP_GetDoctorAvailabiltyByStoreByDateByDoctorDataContext context = new SP_GetDoctorAvailabiltyByStoreByDateByDoctorDataContext();
        public List<SP_GetDoctorAvailabiltyByStoreByDateByDoctorResult> Get(int storeId,int doctorId, DateTime appointmentDate)
        {
            var result = context.SP_GetDoctorAvailabiltyByStoreByDateByDoctor(storeId, doctorId, appointmentDate);
            return result.ToList();
        }
    }
}
