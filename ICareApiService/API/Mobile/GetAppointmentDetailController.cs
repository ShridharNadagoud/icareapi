﻿using ICareApiService.Models;
using ICareApiService.LinqToSql.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ICareApiService.API.Mobile
{
    public class GetAppointmentDetailController : ApiController
    {
        // GET: GetAppointmentDetail
        [Authorize]
        public List<CustomerAppointmentDetail> Get(string id)
        {
            GetAppointmentDetailDataContext _Context = new GetAppointmentDetailDataContext();

            var _appointmentDetail = _Context.SP_GetAppointmentDetail(Convert.ToDateTime(DateTime.Now.ToString("yyyy MM dd")), Convert.ToDateTime(DateTime.Now.AddDays(10).ToString("yyyy MM dd")), id);

            List<CustomerAppointmentDetail> appBookedList = new List<CustomerAppointmentDetail>();
            foreach (var data in _appointmentDetail)
            {
                CustomerAppointmentDetail alAppt = new CustomerAppointmentDetail();
                alAppt.AppointmentID = data.appt_no;
                alAppt.CustomerFirstName = data.first_name;
                alAppt.CustomerLastName = data.last_name;
                alAppt.CustomerID = data.patient_no;
                alAppt.DateOfAppointment = data.appt_date;
                alAppt.StartTime = Convert.ToDateTime(data.appt_start_time).ToString("HH:mm") ?? "0:00";
                alAppt.EndTime = Convert.ToDateTime(data.appt_end_time).ToString("HH:mm");
                alAppt.IsConfirmed = data.appt_confirmed_ind;
                alAppt.ReasonForVisit = data.appt_notes;
                alAppt.CustomerPhoneNumber = data.CustomerPhoneNumber;
                alAppt.CustomerAge = data.CustomerAge;
                alAppt.StoreId = data.LocationID;
                alAppt.StoreName = data.LocationName;
                alAppt.CustomerGender = Convert.ToChar(data.Sex);

                appBookedList.Add(alAppt);
            }
            return appBookedList;
        }
    }
}