﻿using ICareApiService.LinqToSql.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ICareApiService.API.Mobile
{
    [Authorize]
    public class GetPatientDetailsByIdController : ApiController
    {
        GetPatientDetailsByIdDataContext _Context = new GetPatientDetailsByIdDataContext();

        // GET: GetPatientDetailsById
        public List<SP_GetPatientDetailsByIdResult> Get(string companyId, int customerId)
        {
            var patientDetail = _Context.SP_GetPatientDetailsById(companyId, customerId);
            return patientDetail.ToList();
        }
    }
}