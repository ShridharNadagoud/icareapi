﻿using ICareApiService.LinqToSql.Mobile.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetDoctorListByStoreIdController : ApiController
    {
        GetDoctorListByStoreIdDataContext _Context = new GetDoctorListByStoreIdDataContext();
        public List<SP_GetDoctorListByStoreIdResult> Get(int id)
        {
            var doctorList = _Context.SP_GetDoctorListByStoreId(id);
            return doctorList.ToList();
        }
    }
}
