﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetDoctorAvailabilityController : ApiController
    {
        GetDoctorAvailabilityDataContext _Context = new GetDoctorAvailabilityDataContext();

        [Authorize]
        public List<SP_GetDoctorAvailabilityResult> Get(string officeNum, int storeId, DateTime selectedDate)
        {
            var result = _Context.SP_GetDoctorAvailability(officeNum, storeId, selectedDate);
            return result.ToList();
        }
    }
}
