﻿using ICareApiService.LinqToSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetStoreTimingsController : ApiController
    {
        private GetStoreTimingsDataContext _StoreContext = new GetStoreTimingsDataContext();
        public IEnumerable<SP_GetStoreHoursResult> GetStoreTimings(string companyId, int storeID)
        {
            var result=_StoreContext.SP_GetStoreHours(companyId, storeID);
            return result.ToList();
        }
    }
}
