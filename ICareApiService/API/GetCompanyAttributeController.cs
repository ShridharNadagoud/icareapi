﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetCompanyAttributeController : ApiController
    {
        GetCompanyAttributeDataContext _Context = new GetCompanyAttributeDataContext();

       // [Authorize]
        public List<SP_GetCompanyAttributeResult> Get(string requestUrl)
        {
            var result = _Context.SP_GetCompanyAttribute(requestUrl);
            return result.ToList();
        }
    }
}
