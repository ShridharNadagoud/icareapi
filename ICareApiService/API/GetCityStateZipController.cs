﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ICareApiService.API
{
    public class GetCityStateZipController : ApiController
    {
        GetCityStateZipDataContext _context = new GetCityStateZipDataContext();

        //[Authorize]
        public List<SP_GetCityStateZipResult> Get(string text)
        {
            var result = _context.SP_GetCityStateZip(text);
            return result.ToList();
        }
    }
}
