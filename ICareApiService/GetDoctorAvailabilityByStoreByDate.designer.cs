﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ICareApiService
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="ALogicOO ")]
	public partial class GetDoctorAvailabilityByStoreByDateDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public GetDoctorAvailabilityByStoreByDateDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["OptiBot01ConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorAvailabilityByStoreByDateDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorAvailabilityByStoreByDateDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorAvailabilityByStoreByDateDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorAvailabilityByStoreByDateDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.SP_GetDoctorAvailabiltyByStoreByDate")]
		public ISingleResult<SP_GetDoctorAvailabiltyByStoreByDateResult> SP_GetDoctorAvailabiltyByStoreByDate([global::System.Data.Linq.Mapping.ParameterAttribute(Name="StoreId", DbType="Int")] System.Nullable<int> storeId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="SelectedAppointmentDate", DbType="DateTime")] System.Nullable<System.DateTime> selectedAppointmentDate)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), storeId, selectedAppointmentDate);
			return ((ISingleResult<SP_GetDoctorAvailabiltyByStoreByDateResult>)(result.ReturnValue));
		}
	}
	
	public partial class SP_GetDoctorAvailabiltyByStoreByDateResult
	{
		
		private int _DoctorId;
		
		private string _LastName;
		
		private string _FirstName;
		
		private string _SlotTime;
		
		private string _DoctorName;
		
		private int _ExamMinutes;
		
		private string _StorePhoneNumber;
		
		public SP_GetDoctorAvailabiltyByStoreByDateResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DoctorId", DbType="Int NOT NULL")]
		public int DoctorId
		{
			get
			{
				return this._DoctorId;
			}
			set
			{
				if ((this._DoctorId != value))
				{
					this._DoctorId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LastName", DbType="VarChar(50)")]
		public string LastName
		{
			get
			{
				return this._LastName;
			}
			set
			{
				if ((this._LastName != value))
				{
					this._LastName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FirstName", DbType="VarChar(50)")]
		public string FirstName
		{
			get
			{
				return this._FirstName;
			}
			set
			{
				if ((this._FirstName != value))
				{
					this._FirstName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SlotTime", DbType="Char(5)")]
		public string SlotTime
		{
			get
			{
				return this._SlotTime;
			}
			set
			{
				if ((this._SlotTime != value))
				{
					this._SlotTime = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DoctorName", DbType="VarChar(101)")]
		public string DoctorName
		{
			get
			{
				return this._DoctorName;
			}
			set
			{
				if ((this._DoctorName != value))
				{
					this._DoctorName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ExamMinutes", DbType="Int NOT NULL")]
		public int ExamMinutes
		{
			get
			{
				return this._ExamMinutes;
			}
			set
			{
				if ((this._ExamMinutes != value))
				{
					this._ExamMinutes = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_StorePhoneNumber", DbType="VarChar(14)")]
		public string StorePhoneNumber
		{
			get
			{
				return this._StorePhoneNumber;
			}
			set
			{
				if ((this._StorePhoneNumber != value))
				{
					this._StorePhoneNumber = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
