﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICareApiService.Models
{
    public class GetAppointmentDetailsParams
    {
        public string companyRefCode { get; set; }
        public int? storeId { get; set; }
        public int? doctorId { get; set; }
        public DateTime? apptStartDate { get; set; }
        public DateTime? apptEndDate { get; set; }
        public int? apptStatusId { get; set; }
    }
}