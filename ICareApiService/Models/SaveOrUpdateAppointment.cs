﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICareApiService.Models
{
    public class SaveOrUpdateAppointment
    {
        public int? AppointmentId { get; set; }
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }
        public DateTime DateScheduled { get; set; }
        public string TimeScheduled { get; set; }
        public string Duration { get; set; }
        public int DurationMinutes { get; set; }
        public string CustomerId { get; set; }
        public DateTime CustomerDob { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerEmailId { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }

        public string Notes { get; set; }
        public string ReasonForVisit { get; set; }
        public int ReasonForVisitId { get; set; }
        public int apptStatusId { get; set; }
    }
}