﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICareApiService.Models
{
    public class ZipCodeInfo
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }    
        public bool IsValidZipCode { get; set; }
    }
}