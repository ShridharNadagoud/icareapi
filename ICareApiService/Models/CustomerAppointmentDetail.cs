﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICareApiService.Models
{
    public class CustomerAppointmentDetail
    {
        public CustomerAppointmentDetail(int id, int customer, string customerName, string dateOfService, string lastVisitedDoctorID, string nextVisitedDoctorID, DateTime dateOfAppointment, string startTime, string endTime, bool isConfirmed, string doctorScheduleID, string customerFirstName, string customerLastName, string customerPhoneNum, char customerGender, string customerEmailId, string locationId, string customerAge)
        {
            this.AppointmentID = id;
            this.CustomerID = customer;
            //this.CustomerName = customerName;
            this.DateOfService = dateOfService;
            this.LastVisitedDoctorID = lastVisitedDoctorID;
            this.NextVisitedDoctorID = nextVisitedDoctorID;
            this.DateOfAppointment = dateOfAppointment;

            this.StartTime = startTime;
            this.EndTime = endTime;
            this.IsConfirmed = isConfirmed;
            this.DoctorScheduleID = doctorScheduleID;
            this.CustomerFirstName = customerFirstName;
            this.CustomerLastName = customerLastName;
            this.CustomerPhoneNumber = customerPhoneNum;
            this.CustomerGender = customerGender;
            this.CustomerEmailId = customerEmailId;
        }

        public CustomerAppointmentDetail()
        {
        }

        public int AppointmentID { get; set; }

        public int? CustomerID { get; set; }

        public string CustomerName { get; set; }

        public string DateOfService { get; set; }

        public string LastVisitedDoctorID { get; set; }

        public string NextVisitedDoctorID { get; set; }

        public DateTime? DateOfAppointment { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public bool? IsConfirmed { get; set; }

        public string DoctorScheduleID { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public string CustomerPhoneNumber { get; set; }

        public char CustomerGender { get; set; }

        public string CustomerEmailId { get; set; }

        public string ReasonForVisit { get; set; }

        public int? StoreId { get; set; }

        public string StoreName {get; set;}

        public int? CustomerAge { get; set; }
    }
}