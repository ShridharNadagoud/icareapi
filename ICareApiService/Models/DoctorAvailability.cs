﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICareApiService.Models
{
    public class DoctorAvailability
    {
        public string DoctorId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string DoctorName { get; set; }
        public string Slot { get; set; }
        public string ExamMinutes { get; set; }
    }
}