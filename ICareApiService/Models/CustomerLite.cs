﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICareApiService.Models
{
    public class CustomerLite
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ExamOffice { get; set; }
        public string CompanyId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}