﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ICareApiService.LinqToSql.Mobile.ChatBot
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="ALogicOO ")]
	public partial class DeleteAppointmentScheduleDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public DeleteAppointmentScheduleDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["OptiBot01ConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DeleteAppointmentScheduleDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DeleteAppointmentScheduleDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DeleteAppointmentScheduleDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DeleteAppointmentScheduleDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.DeleteAppointmentSchedule")]
		public int DeleteAppointmentSchedule([global::System.Data.Linq.Mapping.ParameterAttribute(Name="CustomerId", DbType="Int")] System.Nullable<int> customerId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="DateOfAppointment", DbType="DateTime")] System.Nullable<System.DateTime> dateOfAppointment, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="StartTime", DbType="NVarChar(50)")] string startTime)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), customerId, dateOfAppointment, startTime);
			return ((int)(result.ReturnValue));
		}
	}
}
#pragma warning restore 1591
