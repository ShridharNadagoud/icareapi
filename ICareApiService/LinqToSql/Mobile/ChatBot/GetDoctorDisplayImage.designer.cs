﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ICareApiService.LinqToSql.Mobile.ChatBot
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="ALogicOO ")]
	public partial class GetDoctorDisplayImageDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public GetDoctorDisplayImageDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["OptiBot01ConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorDisplayImageDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorDisplayImageDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorDisplayImageDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetDoctorDisplayImageDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.GetDoctorDisplayImage")]
		public ISingleResult<GetDoctorDisplayImageResult> GetDoctorDisplayImage([global::System.Data.Linq.Mapping.ParameterAttribute(Name="CustomerDocumentId", DbType="Int")] System.Nullable<int> customerDocumentId)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), customerDocumentId);
			return ((ISingleResult<GetDoctorDisplayImageResult>)(result.ReturnValue));
		}
	}
	
	public partial class GetDoctorDisplayImageResult
	{
		
		private string _FilePath;
		
		public GetDoctorDisplayImageResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FilePath", DbType="VarChar(500)")]
		public string FilePath
		{
			get
			{
				return this._FilePath;
			}
			set
			{
				if ((this._FilePath != value))
				{
					this._FilePath = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
