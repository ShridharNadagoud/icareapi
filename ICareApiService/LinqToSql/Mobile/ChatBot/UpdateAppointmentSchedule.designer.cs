﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ICareApiService.LinqToSql.Mobile.ChatBot
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="ALogicOO ")]
	public partial class UpdateAppointmentScheduleDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public UpdateAppointmentScheduleDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["OptiBot01ConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public UpdateAppointmentScheduleDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UpdateAppointmentScheduleDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UpdateAppointmentScheduleDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UpdateAppointmentScheduleDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.UpdateAppointmentSchedule")]
		public int UpdateAppointmentSchedule([global::System.Data.Linq.Mapping.ParameterAttribute(Name="CustomerId", DbType="Int")] System.Nullable<int> customerId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="PreviousDateOfAppointment", DbType="DateTime")] System.Nullable<System.DateTime> previousDateOfAppointment, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UpdatedDateOfAppointment", DbType="DateTime")] System.Nullable<System.DateTime> updatedDateOfAppointment, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="PreviousStartTime", DbType="NVarChar(50)")] string previousStartTime, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UpdatedStartTime", DbType="NVarChar(50)")] string updatedStartTime, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UpdatedEndTime", DbType="NVarChar(50)")] string updatedEndTime, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UpdatedReasonForVisitId", DbType="Int")] System.Nullable<int> updatedReasonForVisitId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="UpdatedAppointmentNotes", DbType="NVarChar(500)")] string updatedAppointmentNotes)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), customerId, previousDateOfAppointment, updatedDateOfAppointment, previousStartTime, updatedStartTime, updatedEndTime, updatedReasonForVisitId, updatedAppointmentNotes);
			return ((int)(result.ReturnValue));
		}
	}
}
#pragma warning restore 1591
