﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ICareApiService.LinqToSql.Mobile.ChatBot
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="ALogicOO ")]
	public partial class GetCustomerDetailByIdDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public GetCustomerDetailByIdDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["OptiBot01ConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public GetCustomerDetailByIdDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetCustomerDetailByIdDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetCustomerDetailByIdDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public GetCustomerDetailByIdDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.GetCustomerDetailById")]
		public ISingleResult<GetCustomerDetailByIdResult> GetCustomerDetailById([global::System.Data.Linq.Mapping.ParameterAttribute(Name="PatientId", DbType="Int")] System.Nullable<int> patientId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="DoctorId", DbType="Int")] System.Nullable<int> doctorId)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), patientId, doctorId);
			return ((ISingleResult<GetCustomerDetailByIdResult>)(result.ReturnValue));
		}
	}
	
	public partial class GetCustomerDetailByIdResult
	{
		
		private System.Nullable<int> _PatientId;
		
		private string _PatientName;
		
		private System.Nullable<int> _PatientAge;
		
		private string _PatientPhoneNumber;
		
		private string _PatientEmailId;
		
		private string _PatientAddress;
		
		private string _PatientCity;
		
		private System.Nullable<int> _PatientPrescription;
		
		private System.Nullable<System.DateTime> _PatientExamDate;
		
		private string _InsuranceCarrierName;
		
		private string _InsuredId;
		
		private string _InsuredName;
		
		private System.Nullable<System.DateTime> _InsuredDob;
		
		private string _InsRelationship;
		
		private System.Nullable<System.DateTime> _DateOfAppointment;
		
		private System.Nullable<bool> _IsConfirmed;
		
		private string _ApptNotes;
		
		private string _ApptStartTime;
		
		private string _ApptEndTime;
		
		private string _ReasonForVisit;
		
		private string _DoctorName;
		
		private string _CompanyName;
		
		private string _LocationName;
		
		public GetCustomerDetailByIdResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientId", DbType="Int")]
		public System.Nullable<int> PatientId
		{
			get
			{
				return this._PatientId;
			}
			set
			{
				if ((this._PatientId != value))
				{
					this._PatientId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientName", DbType="VarChar(101)")]
		public string PatientName
		{
			get
			{
				return this._PatientName;
			}
			set
			{
				if ((this._PatientName != value))
				{
					this._PatientName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientAge", DbType="Int")]
		public System.Nullable<int> PatientAge
		{
			get
			{
				return this._PatientAge;
			}
			set
			{
				if ((this._PatientAge != value))
				{
					this._PatientAge = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientPhoneNumber", DbType="VarChar(14)")]
		public string PatientPhoneNumber
		{
			get
			{
				return this._PatientPhoneNumber;
			}
			set
			{
				if ((this._PatientPhoneNumber != value))
				{
					this._PatientPhoneNumber = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientEmailId", DbType="VarChar(250)")]
		public string PatientEmailId
		{
			get
			{
				return this._PatientEmailId;
			}
			set
			{
				if ((this._PatientEmailId != value))
				{
					this._PatientEmailId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientAddress", DbType="VarChar(250)")]
		public string PatientAddress
		{
			get
			{
				return this._PatientAddress;
			}
			set
			{
				if ((this._PatientAddress != value))
				{
					this._PatientAddress = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientCity", DbType="VarChar(50)")]
		public string PatientCity
		{
			get
			{
				return this._PatientCity;
			}
			set
			{
				if ((this._PatientCity != value))
				{
					this._PatientCity = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientPrescription", DbType="Int")]
		public System.Nullable<int> PatientPrescription
		{
			get
			{
				return this._PatientPrescription;
			}
			set
			{
				if ((this._PatientPrescription != value))
				{
					this._PatientPrescription = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PatientExamDate", DbType="Date")]
		public System.Nullable<System.DateTime> PatientExamDate
		{
			get
			{
				return this._PatientExamDate;
			}
			set
			{
				if ((this._PatientExamDate != value))
				{
					this._PatientExamDate = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InsuranceCarrierName", DbType="VarChar(250)")]
		public string InsuranceCarrierName
		{
			get
			{
				return this._InsuranceCarrierName;
			}
			set
			{
				if ((this._InsuranceCarrierName != value))
				{
					this._InsuranceCarrierName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InsuredId", DbType="VarChar(250)")]
		public string InsuredId
		{
			get
			{
				return this._InsuredId;
			}
			set
			{
				if ((this._InsuredId != value))
				{
					this._InsuredId = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InsuredName", DbType="VarChar(501)")]
		public string InsuredName
		{
			get
			{
				return this._InsuredName;
			}
			set
			{
				if ((this._InsuredName != value))
				{
					this._InsuredName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InsuredDob", DbType="DateTime")]
		public System.Nullable<System.DateTime> InsuredDob
		{
			get
			{
				return this._InsuredDob;
			}
			set
			{
				if ((this._InsuredDob != value))
				{
					this._InsuredDob = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_InsRelationship", DbType="VarChar(250)")]
		public string InsRelationship
		{
			get
			{
				return this._InsRelationship;
			}
			set
			{
				if ((this._InsRelationship != value))
				{
					this._InsRelationship = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DateOfAppointment", DbType="DateTime")]
		public System.Nullable<System.DateTime> DateOfAppointment
		{
			get
			{
				return this._DateOfAppointment;
			}
			set
			{
				if ((this._DateOfAppointment != value))
				{
					this._DateOfAppointment = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IsConfirmed", DbType="Bit")]
		public System.Nullable<bool> IsConfirmed
		{
			get
			{
				return this._IsConfirmed;
			}
			set
			{
				if ((this._IsConfirmed != value))
				{
					this._IsConfirmed = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ApptNotes", DbType="NVarChar(500)")]
		public string ApptNotes
		{
			get
			{
				return this._ApptNotes;
			}
			set
			{
				if ((this._ApptNotes != value))
				{
					this._ApptNotes = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ApptStartTime", DbType="NVarChar(25)")]
		public string ApptStartTime
		{
			get
			{
				return this._ApptStartTime;
			}
			set
			{
				if ((this._ApptStartTime != value))
				{
					this._ApptStartTime = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ApptEndTime", DbType="NVarChar(25)")]
		public string ApptEndTime
		{
			get
			{
				return this._ApptEndTime;
			}
			set
			{
				if ((this._ApptEndTime != value))
				{
					this._ApptEndTime = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ReasonForVisit", DbType="VarChar(250)")]
		public string ReasonForVisit
		{
			get
			{
				return this._ReasonForVisit;
			}
			set
			{
				if ((this._ReasonForVisit != value))
				{
					this._ReasonForVisit = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DoctorName", DbType="VarChar(101)")]
		public string DoctorName
		{
			get
			{
				return this._DoctorName;
			}
			set
			{
				if ((this._DoctorName != value))
				{
					this._DoctorName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CompanyName", DbType="VarChar(50)")]
		public string CompanyName
		{
			get
			{
				return this._CompanyName;
			}
			set
			{
				if ((this._CompanyName != value))
				{
					this._CompanyName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LocationName", DbType="VarChar(251)")]
		public string LocationName
		{
			get
			{
				return this._LocationName;
			}
			set
			{
				if ((this._LocationName != value))
				{
					this._LocationName = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
