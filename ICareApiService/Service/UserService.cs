﻿using ICareApiService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ICareApiService.Service
{
    public class UserService
    {

        public User GetUserByCredentials(string email, string password)
        {

            DoctorTableDataContext _Context = new DoctorTableDataContext();
            var doctorDetail = (from e in _Context.Doctors 
                                where e.UserName == email && e.Password == password
                                select new
                                {
                                    e.ID,
                                    e.UserName,
                                    e.Password,
                                    e.FirstName
                                }
            ).FirstOrDefault();

            CustomerTableDataContext _customerContext = new CustomerTableDataContext();

            var customerDetails = (from e in _customerContext.Customers
                                   where e.EmailID == email && e.Password == password
                                   select new
                                   {
                                       e.ID,
                                       e.EmailID,
                                       e.Password,
                                       e.FirstName,
                                       e.LastName
                                   }).FirstOrDefault();

            User user = new User();

            if (doctorDetail != null)
            {
                user.Id = doctorDetail.ID.ToString();
                user.Email = doctorDetail.UserName;
                user.Password = doctorDetail.Password;
                user.Name = doctorDetail.FirstName;
            }
            else if (customerDetails != null)
            {
                user.Id = customerDetails.ID.ToString();
                user.Email = customerDetails.EmailID;
                user.Password = customerDetails.Password;
                user.Name = customerDetails.FirstName+" "+customerDetails.LastName;
            }
            else
            {
                user = null;

            }

            if (user != null)
            {
                user.Password = string.Empty;
            }
            return user;
        }
    }
}