﻿using GoogleMaps.LocationServices;
using ICareApiService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;

namespace ICareApiService.Service
{
    public class ZipcodeService
    {

        public ZipCodeInfo CalculateLatitudeAndLongitude(string zipcode)
        {
            WebClient client = new WebClient();
            ZipCodeInfo myoject = new ZipCodeInfo();
            try
            {
                var url = ConfigurationManager.AppSettings["GoogleApi"] + zipcode + "&key="+ ConfigurationManager.AppSettings["GoogleApiKey"];
                var dataResponse = client.DownloadString(url);
                var getvalue = JObject.Parse(dataResponse);
                var status = (string)getvalue["status"];
                if (status.Equals("OK"))
                {
                    var latitude = (string)getvalue["results"][0]["geometry"]["location"]["lat"];
                    var longitude = (string)getvalue["results"][0]["geometry"]["location"]["lat"];
                    myoject.Latitude = latitude.ToString();
                    myoject.Longitude = longitude.ToString();
                    myoject.IsValidZipCode = true;
                }
                else
                {
                    myoject.IsValidZipCode = false;
                }
               
            }
            catch (Exception ex)
            {              
            }
          return myoject;
        }
    }    
}