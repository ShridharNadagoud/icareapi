﻿CREATE TABLE [dbo].[ClientConversation] (
    [ID]       INT           IDENTITY (1, 1) NOT NULL,
    [Response] VARCHAR (MAX) NULL,
    [Reply]    VARCHAR (MAX) NULL,
    CONSTRAINT [PK_Id] PRIMARY KEY CLUSTERED ([ID] ASC)
);

