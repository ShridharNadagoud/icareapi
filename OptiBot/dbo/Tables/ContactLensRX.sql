﻿CREATE TABLE [dbo].[ContactLensRX] (
    [ID]                     INT            IDENTITY (1, 1) NOT NULL,
    [RightBase]              DECIMAL (5, 3) NULL,
    [RightDiameter]          DECIMAL (4, 1) NULL,
    [RightSphere]            DECIMAL (4, 2) NULL,
    [RightCylinder]          DECIMAL (4, 2) NULL,
    [RightAxis]              INT            NULL,
    [RightAdd1]              DECIMAL (3, 2) NULL,
    [LeftBase]               DECIMAL (5, 3) NULL,
    [LeftDiameter]           DECIMAL (4, 1) NULL,
    [LeftSphere]             DECIMAL (4, 2) NULL,
    [LeftCylinder]           DECIMAL (4, 2) NULL,
    [LeftAxis]               INT            NULL,
    [LeftAdd1]               DECIMAL (3, 2) NULL,
    [ContactLensDescription] VARCHAR (100)  NULL,
    [LensQty]                INT            NULL,
    [ExamDate]               DATETIME       NULL,
    [DoctorID]               INT            NULL,
    [CustomerExamID]         INT            NULL,
    [RxType]                 INT            NULL
);

