﻿CREATE TABLE [dbo].[Doctor_Info] (
    [Doctor_Id]            INT           IDENTITY (1, 1) NOT NULL,
    [Doctor_Name]          VARCHAR (50)  NOT NULL,
    [Doctor_Specializtion] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [Doctor_Id] PRIMARY KEY CLUSTERED ([Doctor_Id] ASC)
);

