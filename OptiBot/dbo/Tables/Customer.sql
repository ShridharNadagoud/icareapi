﻿CREATE TABLE [dbo].[Customer] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [Uuid]        VARCHAR (500) NULL,
    [FirstName]   VARCHAR (50)  NULL,
    [LastName]    VARCHAR (50)  NULL,
    [PhoneNumber] VARCHAR (14)  NULL,
    [DateOfBirth] DATE          NULL,
    [Gender]      CHAR (1)      NULL,
    [BotID]       BIGINT        NULL,
    [EmailID]     VARCHAR (250) NULL,
    [FacebookID]  BIGINT        NULL,
    [Age]         INT           NULL,
    [CompanyID]   INT           NULL,
    [StoreID]     INT           NULL,
    [ClientKey]   VARCHAR (50)  NULL,
    [RxImage]     IMAGE         NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
GRANT ALTER
    ON OBJECT::[dbo].[Customer] TO PUBLIC
    WITH GRANT OPTION
    AS [dbo];


GO
DENY DELETE
    ON OBJECT::[dbo].[Customer] TO PUBLIC
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[Customer] TO PUBLIC
    WITH GRANT OPTION
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[Customer] TO PUBLIC
    WITH GRANT OPTION
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[Customer] TO PUBLIC
    WITH GRANT OPTION
    AS [dbo];


GO
GRANT ALTER
    ON OBJECT::[dbo].[Customer] TO [guest]
    WITH GRANT OPTION
    AS [dbo];


GO
DENY DELETE
    ON OBJECT::[dbo].[Customer] TO [guest]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[Customer] TO [guest]
    WITH GRANT OPTION
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[Customer] TO [guest]
    WITH GRANT OPTION
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[Customer] TO [guest]
    WITH GRANT OPTION
    AS [dbo];

