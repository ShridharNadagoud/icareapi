﻿CREATE TABLE [dbo].[MessageMaster] (
    [ID]      INT           IDENTITY (1, 1) NOT NULL,
    [Message] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([ID] ASC)
);

