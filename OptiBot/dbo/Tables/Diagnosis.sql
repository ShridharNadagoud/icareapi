﻿CREATE TABLE [dbo].[Diagnosis] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Diagnosis] PRIMARY KEY CLUSTERED ([ID] ASC)
);

