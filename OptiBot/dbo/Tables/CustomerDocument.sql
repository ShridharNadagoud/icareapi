﻿CREATE TABLE [dbo].[CustomerDocument] (
    [ID]                  INT             IDENTITY (1, 1) NOT NULL,
    [CustomerId]          INT             NULL,
    [CustomerExamId]      INT             NULL,
    [Document]            VARBINARY (MAX) NOT NULL,
    [DocumentName]        VARCHAR (50)    NOT NULL,
    [DocumentContentType] VARCHAR (50)    NULL,
    [DocumentPath]        VARCHAR (500)   NULL,
    CONSTRAINT [PK_CustomerDocument] PRIMARY KEY CLUSTERED ([ID] ASC)
);

