﻿CREATE TABLE [dbo].[InsuranceCompany] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [CarrierName] VARCHAR (250) NULL,
    [PhoneNumber] VARCHAR (250) NULL,
    [Isactive]    BIT           NULL,
    [CompanyId]   VARCHAR (250) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

