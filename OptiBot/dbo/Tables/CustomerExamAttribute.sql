﻿CREATE TABLE [dbo].[CustomerExamAttribute] (
    [CustomerExamId] INT           NOT NULL,
    [AttributeId]    INT           NOT NULL,
    [AttributeValue] VARCHAR (200) NOT NULL
);

