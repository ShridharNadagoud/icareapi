﻿CREATE TABLE [dbo].[EyeglassRx] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [RightFPD]      DECIMAL (3, 1) NULL,
    [RightNPD]      DECIMAL (3, 1) NULL,
    [RightSPH]      DECIMAL (4, 2) NULL,
    [RightCYL]      DECIMAL (4, 2) NULL,
    [RightAxis]     DECIMAL (4, 2) NULL,
    [RightVA]       VARCHAR (5)    NULL,
    [RightAdd]      DECIMAL (3, 2) NULL,
    [LeftFPD]       DECIMAL (3, 1) NULL,
    [LeftNPD]       DECIMAL (3, 1) NULL,
    [LeftSPH]       DECIMAL (4, 2) NULL,
    [LeftCYL]       DECIMAL (4, 2) NULL,
    [LeftAxis]      DECIMAL (4, 2) NULL,
    [LeftVA]        VARCHAR (5)    NULL,
    [LeftAdd]       DECIMAL (3, 2) NULL,
    [RxType]        INT            NULL,
    [RxMemo]        VARCHAR (100)  NULL,
    [PatientExamId] INT            NULL,
    [HO]            NVARCHAR (500) NULL,
    [CO]            NVARCHAR (500) NULL,
    [Diagnosis]     NVARCHAR (100) NULL,
    [Advise]        NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

