﻿CREATE TABLE [dbo].[CompanyAttribute] (
    [CompanyId]      INT            NOT NULL,
    [AttributeName]  NVARCHAR (50)  NOT NULL,
    [AttributeValue] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_CompanyAttribute] PRIMARY KEY CLUSTERED ([CompanyId] ASC, [AttributeName] ASC)
);

