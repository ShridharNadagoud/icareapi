﻿CREATE TABLE [dbo].[AppointmentBuilder] (
    [ID]                           INT           IDENTITY (1, 1) NOT NULL,
    [DoctorID]                     INT           NOT NULL,
    [WorkStartTime]                NVARCHAR (25) NULL,
    [WorkEndTime]                  NVARCHAR (25) NULL,
    [AppointmentTimeSpanInMinutes] INT           NULL,
    [DayOfWeek]                    NVARCHAR (25) NULL,
    [Break1StartTime]              NVARCHAR (25) NULL,
    [Break1EndTime]                NVARCHAR (25) NULL,
    [Break2StartTime]              NVARCHAR (25) NULL,
    [Break2EndTime]                NVARCHAR (25) NULL,
    [Break3StartTime]              NVARCHAR (25) NULL,
    [Break3EndTime]                NVARCHAR (25) NULL,
    [Break4StartTime]              NVARCHAR (25) NULL,
    [Break4EndTime]                NVARCHAR (25) NULL,
    [Break5StartTime]              NVARCHAR (25) NULL,
    [Break5EndTime]                NVARCHAR (25) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK__Appointme__Docto__02FC7413] FOREIGN KEY ([DoctorID]) REFERENCES [dbo].[Doctor] ([ID])
);

