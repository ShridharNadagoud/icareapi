﻿CREATE TABLE [dbo].[Appointment] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [CustomerID]          INT            NOT NULL,
    [DateOfService]       DATETIME       NULL,
    [LastVisitedDoctorID] INT            NULL,
    [NextVisitedDoctorID] INT            NULL,
    [DateOfAppointment]   DATETIME       NULL,
    [StartTime]           NVARCHAR (25)  NULL,
    [EndTime]             NVARCHAR (25)  NULL,
    [IsConfirmed]         BIT            NOT NULL,
    [ReasonForVisitId]    INT            NULL,
    [ReasonForVisit]      NVARCHAR (500) NULL,
    [DoctorScheduleID]    INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK__Appointme__LastV__01142BA1] FOREIGN KEY ([LastVisitedDoctorID]) REFERENCES [dbo].[Doctor] ([ID]),
    CONSTRAINT [FK__Appointme__NextV__02084FDA] FOREIGN KEY ([NextVisitedDoctorID]) REFERENCES [dbo].[Doctor] ([ID])
);

