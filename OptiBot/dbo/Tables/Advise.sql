﻿CREATE TABLE [dbo].[Advise] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (500) NOT NULL,
    CONSTRAINT [PK_Advise] PRIMARY KEY CLUSTERED ([ID] ASC)
);

