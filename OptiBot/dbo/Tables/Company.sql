﻿CREATE TABLE [dbo].[Company] (
    [ID]                INT             IDENTITY (1, 1) NOT NULL,
    [CompanyName]       VARCHAR (50)    NULL,
    [CompanyFacebookID] INT             NULL,
    [CompanyLogo]       IMAGE           NULL,
    [HTTPAddress]       NVARCHAR (2000) NULL,
    [Active]            BIT             CONSTRAINT [DF_Company_Active] DEFAULT ((0)) NOT NULL,
    [CompanyId]         VARCHAR (50)    NULL,
    [CompanyRefCode]    NVARCHAR (50)   NULL,
    CONSTRAINT [PK__Company__3214EC273B01D499] PRIMARY KEY CLUSTERED ([ID] ASC)
);

