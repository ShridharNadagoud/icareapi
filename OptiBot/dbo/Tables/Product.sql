﻿CREATE TABLE [dbo].[Product] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [Name]          VARCHAR (200) NOT NULL,
    [ProductTypeId] INT           NOT NULL,
    [Active]        BIT           CONSTRAINT [DF_Product_Active] DEFAULT ((1)) NOT NULL,
    [CompanyId]     INT           NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([Id] ASC)
);

