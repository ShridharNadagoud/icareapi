﻿CREATE TABLE [dbo].[InsuranceDetails] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]       VARCHAR (250) NULL,
    [LastName]        VARCHAR (250) NULL,
    [CustomerId]      INT           NULL,
    [InsuranceNumber] VARCHAR (250) NULL,
    [InsuranceId]     INT           NULL,
    [RelationShip]    VARCHAR (250) NULL,
    [DateOfBirth]     DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

