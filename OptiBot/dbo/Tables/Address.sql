﻿CREATE TABLE [dbo].[Address] (
    [ID]          INT             IDENTITY (1, 1) NOT NULL,
    [AddressType] INT             NOT NULL,
    [ReferenceID] INT             NOT NULL,
    [Address1]    VARCHAR (250)   NULL,
    [Address2]    VARCHAR (250)   NULL,
    [State]       VARCHAR (50)    NULL,
    [City]        VARCHAR (50)    NULL,
    [ZipCode]     VARCHAR (30)    NULL,
    [Country]     VARCHAR (200)   NULL,
    [Latitude]    DECIMAL (18, 7) NULL,
    [Longitude]   DECIMAL (18, 7) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UC_Address] UNIQUE NONCLUSTERED ([AddressType] ASC, [ReferenceID] ASC)
);

