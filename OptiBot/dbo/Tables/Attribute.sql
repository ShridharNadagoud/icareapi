﻿CREATE TABLE [dbo].[Attribute] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [AttributeTypeId]     INT           NOT NULL,
    [Name]                VARCHAR (200) NOT NULL,
    [ListOrderType]       INT           CONSTRAINT [DF_Attribute_ListOrder] DEFAULT ((0)) NOT NULL,
    [DisplayOrderForList] INT           CONSTRAINT [DF_Attribute_DisplayOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Attribute] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1=Medical Condition (variable layout), 2 = Rx Details (Fixed), 3=Diagnosis and Advice (Variable)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Attribute', @level2type = N'COLUMN', @level2name = N'ListOrderType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This will be used for list items of ListOrdeType by ascending order. Example is 1 = Medical Cnd will have Cornia (1), Pupil (2) etc, which the header names for drop down', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Attribute', @level2type = N'COLUMN', @level2name = N'DisplayOrderForList';

