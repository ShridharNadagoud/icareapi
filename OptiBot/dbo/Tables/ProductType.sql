﻿CREATE TABLE [dbo].[ProductType] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]   VARCHAR (200) NOT NULL,
    [Active] BIT           CONSTRAINT [DF_ProductType_Active] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

