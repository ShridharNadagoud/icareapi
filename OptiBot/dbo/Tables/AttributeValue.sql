﻿CREATE TABLE [dbo].[AttributeValue] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [AttributeId]     INT           NOT NULL,
    [AttributeTypeId] INT           NOT NULL,
    [AttributeValue]  VARCHAR (200) NOT NULL,
    [DisplayOrder]    INT           CONSTRAINT [DF_AttributeValue_DisplayOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AttributeValue] PRIMARY KEY CLUSTERED ([Id] ASC)
);

