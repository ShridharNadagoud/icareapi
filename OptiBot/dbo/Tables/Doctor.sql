﻿CREATE TABLE [dbo].[Doctor] (
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]      VARCHAR (50)  NULL,
    [LastName]       VARCHAR (50)  NULL,
    [PhoneNumber]    VARCHAR (14)  NULL,
    [Specialization] VARCHAR (250) NULL,
    [CompanyId]      INT           NULL,
    [Picture]        IMAGE         NULL,
    [UserName]       VARCHAR (500) NULL,
    [Password]       VARCHAR (50)  NULL,
    [EmployeeId]     INT           NULL,
    CONSTRAINT [PK__Doctor__3214EC271D7837F6] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UQ__Doctor__C9F28456D5C339F8] UNIQUE NONCLUSTERED ([UserName] ASC)
);

