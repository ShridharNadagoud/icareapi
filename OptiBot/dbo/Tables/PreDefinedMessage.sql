﻿CREATE TABLE [dbo].[PreDefinedMessage] (
    [ID]       INT             IDENTITY (1, 1) NOT NULL,
    [Response] VARCHAR (MAX)   NULL,
    [Reply]    VARCHAR (MAX)   NULL,
    [Image1]   VARCHAR (MAX)   NULL,
    [Image2]   VARCHAR (MAX)   NULL,
    [Image3]   VARCHAR (MAX)   NULL,
    [price1]   DECIMAL (18, 2) NULL,
    [price2]   DECIMAL (18, 2) NULL,
    [price3]   DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_TestTable] PRIMARY KEY CLUSTERED ([ID] ASC)
);

