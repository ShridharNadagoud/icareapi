﻿CREATE TABLE [dbo].[Doctor_Appointment_Slot] (
    [ID]        INT           NOT NULL,
    [DoctorId]  INT           NULL,
    [SlotNo]    INT           NULL,
    [StartTime] NVARCHAR (50) NULL,
    [EndTime]   NVARCHAR (50) NULL,
    [IsActive]  BIT           NULL,
    [StartDate] DATE          NULL,
    [EndDate]   DATE          NULL,
    CONSTRAINT [PK_Doctor_Appointment_Slot] PRIMARY KEY CLUSTERED ([ID] ASC)
);

