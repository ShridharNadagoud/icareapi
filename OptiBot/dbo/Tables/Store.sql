﻿CREATE TABLE [dbo].[Store] (
    [ID]                     INT           IDENTITY (1, 1) NOT NULL,
    [Name]                   VARCHAR (200) NULL,
    [CompanyID]              INT           NULL,
    [AddressID]              INT           NULL,
    [PhoneNumber]            VARCHAR (200) NULL,
    [StoreFacebookID]        INT           NULL,
    [StoreLogo]              IMAGE         NULL,
    [IsActive]               BIT           NULL,
    [StartDate]              DATE          NULL,
    [ClosedDate]             DATE          NULL,
    [StoreTypeID]            INT           NULL,
    [TimeZone]               INT           NULL,
    [IsDaylightSaving]       BIT           NULL,
    [IsActiveForAppointment] BIT           NULL,
    [ClientKey]              VARCHAR (50)  NULL,
    CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED ([ID] ASC)
);

