﻿CREATE TABLE [dbo].[CustomerExam] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [PatientId]    INT            NULL,
    [DoctorId]     INT            NULL,
    [ExamDate]     DATE           NULL,
    [EyeglassRxId] INT            NULL,
    [ProductId]    INT            NULL,
    [ExpireDate]   DATE           NULL,
    [Remarks]      VARCHAR (2000) NULL,
    CONSTRAINT [PK__Customer__3214EC278C62F323] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK__CustomerE__Docto__04E4BC85] FOREIGN KEY ([DoctorId]) REFERENCES [dbo].[Doctor] ([ID]),
    CONSTRAINT [FK__CustomerE__Patie__03F0984C] FOREIGN KEY ([PatientId]) REFERENCES [dbo].[Customer] ([ID])
);

