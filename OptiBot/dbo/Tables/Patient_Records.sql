﻿CREATE TABLE [dbo].[Patient_Records] (
    [Patient_Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Recipient_Id]        VARCHAR (MAX) NULL,
    [Patient_FirstName]   VARCHAR (50)  NOT NULL,
    [Patient_LastName]    VARCHAR (50)  NULL,
    [Patient_Address]     VARCHAR (MAX) NULL,
    [ZipCode]             INT           NULL,
    [Patient_PhoneNumber] BIGINT        NULL,
    [Date_Of_Service]     DATETIME      NULL,
    [last_VisitDoctorId]  INT           NULL,
    [Appointment_Date]    DATETIME      NULL,
    [Next_Visit_DoctorId] INT           NULL,
    [time_slot_from]      VARCHAR (50)  NULL,
    [time_slot_to]        VARCHAR (50)  NULL,
    CONSTRAINT [PK_Patient_Records] PRIMARY KEY CLUSTERED ([Patient_Id] ASC)
);

