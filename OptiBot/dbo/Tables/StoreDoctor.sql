﻿CREATE TABLE [dbo].[StoreDoctor] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [StoreID]  INT NULL,
    [DoctorID] INT NULL,
    CONSTRAINT [PK_StoreDoctor] PRIMARY KEY CLUSTERED ([Id] ASC)
);

