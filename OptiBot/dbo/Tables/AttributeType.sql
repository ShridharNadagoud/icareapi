﻿CREATE TABLE [dbo].[AttributeType] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_AttributeType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

