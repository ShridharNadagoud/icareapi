﻿CREATE TABLE [dbo].[Doctor_Availability] (
    [Avail_Id]                INT      IDENTITY (1, 1) NOT NULL,
    [Doctor_Id]               INT      NOT NULL,
    [Availability_Time_From1] DATETIME NOT NULL,
    [Availability_Time_To1]   DATETIME NOT NULL,
    [Availability_Time_From2] DATETIME NOT NULL,
    [Availability_Time_To2]   DATETIME NOT NULL,
    [Availability_Time_From3] DATETIME NOT NULL,
    [Availability_Time_To3]   DATETIME NOT NULL,
    [Availability_Time_From4] DATETIME NOT NULL,
    [Availability_Time_To4]   DATETIME NOT NULL,
    [Available_Day]           DATETIME NOT NULL,
    CONSTRAINT [pk_Avail_Id] PRIMARY KEY CLUSTERED ([Avail_Id] ASC)
);

