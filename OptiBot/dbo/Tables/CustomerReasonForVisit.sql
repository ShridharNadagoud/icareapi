﻿CREATE TABLE [dbo].[CustomerReasonForVisit] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [ReasonForVisit]  VARCHAR (250)   NULL,
    [MessageMasterId] INT             NULL,
    [StoreID]         INT             NULL,
    [ChargeForVisit]  DECIMAL (10, 2) NULL,
    [ClientKey]       VARCHAR (50)    NULL,
    CONSTRAINT [PK_CustomerReasonForVisit] PRIMARY KEY CLUSTERED ([ID] ASC)
);

