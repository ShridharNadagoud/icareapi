﻿CREATE TABLE [dbo].[EyeglassRxImage] (
    [FileName]    NVARCHAR (MAX) NULL,
    [FilePath]    NVARCHAR (MAX) NULL,
    [ContentType] NVARCHAR (50)  NOT NULL
);

