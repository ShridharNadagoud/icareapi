﻿CREATE TABLE [dbo].[MessageDetails] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [MessageId]    INT           NOT NULL,
    [Message]      VARCHAR (MAX) NOT NULL,
    [MessageValue] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_MessageDetails] PRIMARY KEY CLUSTERED ([ID] ASC)
);

