﻿CREATE PROCEDURE [dbo].[Sp_SlotAvailability] 
@doctorid int NULL, 
@DateOfappointment datetime NULL
AS
Begin
SELECT * FROM Doctor_Appointment_Slot where DoctorId=@doctorid AND ID NOT IN
(select DoctorScheduleID from Appointment where NextVisitedDoctorID = @doctorid and DateOfAppointment = @DateOfappointment and IsConfirmed = 1)
End