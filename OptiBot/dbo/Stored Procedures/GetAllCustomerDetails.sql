﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllCustomerDetails] 
	-- Add the parameters for the stored procedure here
	@CompanyId INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT cus.ID as PatientId, (cus.FirstName + ' ' + Cus.LastName) as PatientName,cus.PhoneNumber as PatientPhoneNumber,ad.City as PatientCity 
	FROM Customer cus INNER JOIN 
	Address ad on cus.ID = ad.ReferenceID and ad.AddressType = 1 
	WHERE cus.CompanyID = @CompanyId ORDER BY cus.FirstName 

END