﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SP_GetCompanyAttribute 
	-- Add the parameters for the stored procedure here
	@CompanyId VARCHAR(50)
AS
BEGIN
	DECLARE @Id INT

	SET @Id = (SELECT ID FROM Company WHERE CompanyId = @CompanyId) 
	
	IF (@Id > 0)
	SELECT c.CompanyId,ca.AttributeName,ca.AttributeValue FROM CompanyAttribute ca 
	INNER JOIN Company c ON c.ID = ca.CompanyId
	WHERE ca.CompanyId = @Id
END
