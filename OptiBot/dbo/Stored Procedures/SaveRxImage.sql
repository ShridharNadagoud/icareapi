﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SaveRxImage] 
	-- Add the parameters for the stored procedure here
--	@CustomerId INT,
	@Document VARBINARY(Max),
	@DocumentName VARCHAR(50),
	@DocumentContentType VARCHAR(50),
	@DocumentPath VARCHAR(200) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO CustomerDocument([CustomerId],[CustomerExamId],[Document],[DocumentName],[DocumentContentType],[DocumentPath]) 
	VALUES (0,0,@Document,@DocumentName,@DocumentContentType,@DocumentPath)	

	Select @@identity as 'CustomerDocumentId'
END