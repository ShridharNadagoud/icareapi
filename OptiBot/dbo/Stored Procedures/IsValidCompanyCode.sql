﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[IsValidCompanyCode]
@companyRefCode as varchar(20)
AS
	BEGIN
	 If exists( Select * from Company where CompanyRefCode = rtrim(ltrim(@companyRefCode))) Select '1'
	 Else Select '0'
	End

	