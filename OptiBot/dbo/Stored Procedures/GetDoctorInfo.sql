﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetDoctorInfo] 
	-- Add the parameters for the stored procedure here
	--@CompanyRefCode as Nvarchar(20),    --Comment the employee id and uncomment the CompanyRefcode
    @CompanyRefCode as varchar(20),
	@EmployeeId INT, 
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50)
AS
	BEGIN
	
	    DECLARE @attributeName VARCHAR(20) = 'IsReadOnlyApp'
		if (@CompanyRefCode <> '')
		BEGIN
			SELECT d.ID,d.FirstName,d.LastName,(d.FirstName + ' ' + d.LastName) as DoctorName, d.PhoneNumber,d.Specialization,d.CompanyId,d.UserName,d.Password,c.CompanyName, ca.AttributeValue
			FROM Doctor d
			INNER JOIN Company c on c.ID = d.CompanyId 
			INNER JOIN CompanyAttribute ca on c.ID = ca.CompanyId
			WHERE c.CompanyRefCode = @CompanyRefCode and d.UserName like @UserName AND d.Password = @Password AND ca.AttributeName = @attributeName
	    END
		ELSE
		BEGIN
			SELECT d.ID,d.FirstName,d.LastName,(d.FirstName + ' ' + d.LastName) as DoctorName, d.PhoneNumber,d.Specialization,d.CompanyId,d.UserName,d.Password,c.CompanyName, ca.AttributeValue 
			FROM Doctor d
			INNER JOIN Company c on c.ID = d.CompanyId 
			INNER JOIN CompanyAttribute ca on c.ID = ca.CompanyId
			WHERE  d.ID = @EmployeeId AND d.UserName like @UserName AND d.Password = @Password AND ca.AttributeName = @attributeName
	    END

	END
