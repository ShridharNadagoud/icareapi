﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetDoctorLogin] 
	-- Add the parameters for the stored procedure here
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50)
AS
	BEGIN
		SELECT  top 1 ID,(d.FirstName + ' ' + d.LastName) as DoctorName
		FROM Doctor d		
		WHERE  d.UserName like @UserName AND d.Password = @Password
	END