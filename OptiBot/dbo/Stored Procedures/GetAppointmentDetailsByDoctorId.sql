﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAppointmentDetailsByDoctorId]
	-- Add the parameters for the stored procedure here
	@DoctorId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	SELECT a.ID AS AppointmentID
	,c.ID AS CustomerID
	,a.DateOfService
	,a.LastVisitedDoctorID
	,a.NextVisitedDoctorID
	,a.DateOfAppointment
	,a.StartTime
	,a.EndTime
	,a.IsConfirmed
	,a.DoctorScheduleID
	,c.FirstName as CustomerFirstName
	,c.LastName AS CustomerLastName
	,c.PhoneNumber AS CustomerPhoneNumber
	,c.Gender AS CustomerGender
	,c.Age as CustomerAge
	,c.EmailID AS CustomerEmailId
	, crv.ReasonForVisit
	, com.CompanyName 
	, com.CompanyName as StoreName --TBD As of now it is not by store level, it is by company level.so just to see the  in the UI we are displaying the CompanyName as of now. 
	FROM Appointment a 
	INNER JOIN Customer c ON c.ID = a.CustomerID
	INNER JOIN Company com ON com.ID = c.CompanyID  
	INNER JOIN CustomerReasonForVisit crv on a.ReasonForVisitId = crv.ID where a.NextVisitedDoctorID = @DoctorId AND a.IsConfirmed = 1 order by a.ID desc
	
END

