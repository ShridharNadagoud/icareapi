﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCustomerDetailById] 
	
	@PatientId int
	
AS
BEGIN
(
	--Declare @CompanyId int
		SELECT  cus.ID as PatientId
	,(cus.FirstName + ' ' + Cus.LastName) as PatientName
	,cus.Age as PatientAge
	,cus.PhoneNumber as PatientPhoneNumber
	,cus.EmailID as PatientEmailId
	,ad.Address1 as PatientAddress
	,ad.City as PatientCity
	,(select count(*) from CustomerExam where PatientId = @PatientId) as PatientPrescription
	,exam.ExamDate as PatientExamDate
	--,exam.EyeglassRxId as RxId
	--,isnull(rx.RightFPD,0) as RightFPD
	--,isnull(rx.RightNPD,0) as RightNPD
	--,isnull(rx.LeftFPD,0) as LeftFPD
	--,isnull(rx.LeftNPD,0) as LeftNPD
	--,isnull(rx.RightSPH,0) as RightSphere
	--,isnull(rx.LeftSPH,0) as LeftSphere
	--,isnull(rx.RightCYL,0) as RightCylinder
	--,isnull(rx.LeftCYL,0) as LeftCylinder
	--,isnull(rx.RightAxis,0) as RightAxis
	--,isnull(rx.LeftAxis,0) as LeftAxis
	--,isnull(rx.RightVA,0) as RightVA
	--,isnull(rx.LeftVA,0) as LeftVA
	--,isnull(rx.RightAdd,0) as RightAdd
	--,isnull(rx.LeftAdd,0) as LeftAdd
	--,isnull(rx.RxType,0) as RxType
	--,isnull(rx.RxMemo,'nil') as RxMemo
	,inscom.CarrierName as InsuranceCarrierName
	,insdet.InsuranceNumber as InsuredId
	,(insdet.FirstName + ' ' + insdet.LastName) as InsuredName
	,insdet.DateOfBirth as InsuredDob
	,insdet.RelationShip as InsRelationship
	,null as DateOfAppointment
	,null as IsConfirmed
	,null as ApptNotes
	,null as ApptStartTime
	,null as ApptEndTime
	,null as ReasonForVisit
	,(doc.FirstName + ' ' + doc.LastName) as DoctorName
	,com.CompanyName as CompanyName -- TBD should display storewise
	,(compAddr.City + ',' + compAddr.Country) as LocationName

			FROM Customer cus  
			LEFT OUTER JOIN  Address ad ON cus.ID = ad.ReferenceID and ad.AddressType = 1
			LEFT OUTER JOIN CustomerExam exam ON exam.PatientId = @PatientId
			LEFT OUTER JOIN  Doctor doc ON doc.ID = exam.DoctorId
			LEFT OUTER JOIN  Company com ON com.ID = doc.CompanyId
			LEFT OUTER JOIN  Address compAddr ON compAddr.ReferenceID = com.ID and compAddr.AddressType = 3
			LEFT OUTER JOIN EyeglassRx rx on rx.ID = exam.EyeglassRxId
			LEFT OUTER JOIN InsuranceDetails insdet on insdet.CustomerId = @PatientId
			LEFT OUTER JOIN InsuranceCompany inscom on inscom.CompanyId = com.ID --This is Compnay ID TBD to be refactored
			WHERE cus.ID = @PatientId
			
)
	UNION
(
	SELECT	null,null,null,null,null,null,null,null,null,null,null,null,null,null,
	app.DateOfAppointment as DateOfAppointment,
	app.IsConfirmed,
	app.ReasonForVisit as ApptNotes,
	app.StartTime as ApptStartTime,
	app.EndTime as ApptEndTime,
	crv.ReasonForVisit as ReasonForVisit,
	(doc.FirstName + ' ' + doc.LastName) as DoctorName,
	com.CompanyName as CompanyName,
	(addr.City + ',' + addr.Country) as LocationName
			FROM Appointment app
			LEFT OUTER  JOIN  CustomerReasonForVisit crv ON crv.ID = app.ReasonForVisitId
			LEFT OUTER  JOIN  Doctor doc ON doc.ID = app.NextVisitedDoctorID
			LEFT OUTER  JOIN  Company com ON com.ID = doc.CompanyId
			LEFT OUTER  JOIN  Address addr ON addr.ReferenceID = com.ID and addr.AddressType = 3
			WHERE app.CustomerID = @PatientId
) Order by PatientId desc
END
