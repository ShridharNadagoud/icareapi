﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateNewCompanyDefaultAttributes]
@companyId as Int	
	
AS

	BEGIN

		declare @attributeName as varchar(50)
		declare @attributeValue as varchar(50)

		SET @attributeName = 'ApiLink' ; SET @attributeValue = 'https://myedoo.com/testapi/Api'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

		SET @attributeName = 'AppointmentCreatedBy'; SET @attributeValue = '2452'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

		SET @attributeName = 'CountryCode'; SET @attributeValue = '384'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

		SET @attributeName = 'DefaultOfficeForInsurance' ; SET @attributeValue = '999'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

		SET @attributeName = 'DistanceUnit' ; SET @attributeValue = 'Miles'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

		SET @attributeName = 'DistanceWithinRange' ; SET @attributeValue = '100'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

		SET @attributeName = 'IsReadOnlyApp' ; SET @attributeValue = '0'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

		SET @attributeName = 'ListOfValidReasonIds' ; SET @attributeValue = '1138726'
		If not exists(select * from CompanyAttribute where CompanyId = @companyId and AttributeValue = @attributeName)
		begin
			insert into CompanyAttribute select @companyId,@attributeName,@attributeValue
		end

END