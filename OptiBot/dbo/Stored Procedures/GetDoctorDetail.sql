﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetDoctorDetail] 
	-- Add the parameters for the stored procedure here
	@CompanyCode INT, 
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50)
AS
	BEGIN
		SELECT d.ID,d.FirstName,d.LastName,(d.FirstName + ' ' + d.LastName) as DoctorName, d.PhoneNumber,d.Specialization,d.CompanyId,d.UserName,d.Password,c.CompanyName 
		FROM Doctor d
		INNER JOIN Company c on c.ID = d.CompanyId
		WHERE d.CompanyId = @CompanyCode AND d.UserName like @UserName AND d.Password = @Password
	END
