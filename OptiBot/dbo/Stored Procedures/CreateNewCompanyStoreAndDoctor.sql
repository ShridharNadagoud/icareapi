﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateNewCompanyStoreAndDoctor]
@companyName as varchar(20),
@storeName as varchar(20),
@storeAddress1 as varchar(50),
 @storeAddress2 as varchar(20),
 @storeCity as varchar(20),
 @storePinCode as varchar(20),
 @storeState as varchar(20),
 @storeCountry as varchar(20)	
	
AS
	BEGIN

			declare @companyId as int
			declare @storeId as int
			declare @doctorId as int

			If not exists(select * from Store where CompanyID = @companyId and Name = @storeName)
			begin
				insert into Company select @companyName,459,null, null
				set  @companyId = @@identity
				insert into Address select 3,@companyId,@storeAddress1, @storeAddress2,@storeState,@storeCity,@storePinCode, @storeCountry,null,null
			end
			else
			begin
			  set @companyId = (select top 1 ID from Company where CompanyName = @companyName)
			end

			If not exists(select * from Store where CompanyID = @companyId and Name = @storeName)
			begin
				insert into Store select 'Store-' + @storeName,  @companyId, null,null,null,null,1,getdate(),  getdate()+ 30, null,null,null,1,null
				set @storeId = @@identity
				insert into Address select 4,@storeId,@storeAddress1, @storeAddress2,@storeState,@storeCity,@storePinCode, @storeCountry,null,null
			End
			begin
			  set @storeId = (select top 1 ID from Store where CompanyID = @companyId and Name = @storeName)
			end
			If not exists(select * from Doctor where CompanyId = @companyId and FirstName = 'Eye Doctor' and LastName = 'Test')
			Begin
				insert into Doctor select 'Eye Doctor', 'Test','1234567890', 'Ophthalmologist', @companyId,null,'TestDoctor', 'test123'
				set @doctorId = @@identity
				insert into StoreDoctor select @storeId, @doctorId
				insert into address select 2,@doctorId, 'Dr-' + @storeAddress1, @storeAddress2,@storeState,@storeCity,@storePinCode, @storeCountry,null,null

				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,1,'9:00','9:30',1,'2017/04/11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,2,'9:30','10:00',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,3,'10:00','10:30',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,4,'10:30','11:00', 1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,5,'11:00','11:30',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,6,'11:30','12:00',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,7,'12:00','12:30',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,8,'12:30','13:00',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,9,'13:00','13:30',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,10,'13:30','14:00',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,11,'14:00','14:30',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,12,'14:30','15:00',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,13,'15:00','15:30',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,14,'15:30','16:00',1,'2017-04-11',null)
				--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,15,'16:00','16:30',1,'2017-04-11',null)
				-- INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,16,'16:30','17:00',1,'2017-04-11',null)
				-- INSERT INTO [dbo].[Doctor_Appointment_Slot] ([DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
				-- VALUES(@doctorId,17,'17:00','17:30',1,'2017-04-11',null)
			End
 	END


----Company Name

--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,19,'Brigade Road',null,'KA','Bangalore',560028,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,20,'Marathalli',null,'KA','Bangalore',560065,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,21,'KR Puram',null,'KA','Bangalore',560064,'India',null,null)


----Doctor Name

--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 2',9875641592,'Ophthalmologist',19,'testdoctor2','test234')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 3',7894654555,'Ophthalmologist',19,'testdoctor3','test345')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 4',6998255556,'Ophthalmologist',19,'testdoctor4','test456')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 5',8058110505,'Ophthalmologist',20,'testdoctor5','test567')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 6',8848484848,'Ophthalmologist',20,'testdoctor6','test678')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 7',9897456231,'Ophthalmologist',20,'testdoctor7','test789')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 8',9878465612,'Ophthalmologist',21,'testdoctor8','test890')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 9',6548465846,'Ophthalmologist',21,'testdoctor9','test901')
--Insert Into Doctor(FirstName,LastName,PhoneNumber,Specialization,CompanyId,UserName,Password) VALUES ('Eye','Doctor 10',6546666665,'Ophthalmologist',21,'testdoctor6','test678')

---- Company Address
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,19,'Brigade Road',null,'KA','Bangalore',560028,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,20,'Marathalli',null,'KA','Bangalore',560065,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,21,'KR Puram',null,'KA','Bangalore',560064,'India',null,null)


---- Doctor Address
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (2,7,'Silk Board',null,'KA','Bangalore',560028,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES(2,8,'KundanaHalli',null,'KA','Bangalore',560029,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (2,9,'Koramangala',null,'KA','Bangalore',560030,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (2,10,'Maruthi Nagar',null,'KA','Bangalore',560031,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,11,'MG Road',null,'KA','Bangalore',560032,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,12,'KR Market',null,'KA','Bangalore',560033,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,13,'Electro City',null,'KA','Bangalore',560034,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,14,'Marathalli',null,'KA','Bangalore',5600635,'India',null,null)
--Insert Into Address(AddressType,ReferenceId,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude) VALUES (3,15,'KR Puram',null,'KA','Bangalore',560036,'India',null,null)

--Doctor_Appoint_Slot_ Configuration


--select * from Doctor_Appointment_Slot order by id desc

--declare @doctorId INT
--declare @id INT
--set @doctorId = 15
--set @id = 199

--INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id,@doctorId,1,'09:00','09:30',1,'2017/04/11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+1,@doctorId,2,'9:30','10:00',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+2,@doctorId,3,'10:00','10:30',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+3,@doctorId,4,'10:30','11:00', 1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+4,@doctorId,5,'11:00','11:30',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+5,@doctorId,6,'11:30','12:00',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+6,@doctorId,7,'12:00','12:30',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+7,@doctorId,8,'12:30','13:00',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+8,@doctorId,9,'13:00','13:30',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+9,@doctorId,10,'13:30','14:00',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+10,@doctorId,11,'14:00','14:30',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+11,@doctorId,12,'14:30','15:00',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+12,@doctorId,13,'15:00','15:30',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+13,@doctorId,14,'15:30','16:00',1,'2017-04-11',null)
--				INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+14,@doctorId,15,'16:00','16:30',1,'2017-04-11',null)
--				 INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+15,@doctorId,16,'16:30','17:00',1,'2017-04-11',null)
--				 INSERT INTO [dbo].[Doctor_Appointment_Slot] ([Id],[DoctorId],[SlotNo],[StartTime],[EndTime],[IsActive],[StartDate],[EndDate])
--				 VALUES(@id+16,@doctorId,17,'17:00','17:30',1,'2017-04-11',null)

--select * from Customer order by id desc


----Customer Name

--Insert Into Customer select null,'John','Snow',8056478789,'09-21-1991','M',null,'johnsnow@gmail.com',null,25,19,null,null,null
--Insert Into Customer select null,'Sara','Gilbert',9846517458,'10-10-1988','F',null,'sarah@gmail.com',null,26,19,null,null,null
--Insert Into Customer select null,'Kunal','Nayyar',7845986516,'12-12-1981','M',null,'kunal@gmail.com',null,35,19,null,null,null

--Insert Into Customer select null,'Penny','Cuoco',7465156325,'09-21-1990','F',null,'penny@gmail.com',null,26,20,null,null,null
--Insert Into Customer select null,'Aarti','Mann',9894098940,'10-10-1977','F',null,'aarti@gmail.com',null,40,20,null,null,null
--Insert Into Customer select null,'Bob','Newhart',8046521256,'12-12-1981','M',null,'boby@gmail.com',null,35,20,null,null,null

--Insert Into Customer select null,'John','Ross',84512548963,'07-21-1991','M',null,'johnross@gmail.com',null,25,21,null,null,null
--Insert Into Customer select null,'Laura','Spencer',8471520152,'10-10-1992','F',null,'spencer@gmail.com',null,24,21,null,null,null
--Insert Into Customer select null,'Carol','Ann',4567812055,'12-12-1981','F',null,'carol@gmail.com',null,35,21,null,null,null

----Address 

--Insert Into Address select 1,172,'HSRLayout',null,'KA','Bangalore',560065,'India',null,null
--Insert Into Address select 1,173,'Varthur',null,'KA','Bangalore',560012,'India',null,null
--Insert Into Address select 1,174,'Madiwala',null,'KA','Bangalore',560068,'India',null,null


--Insert Into Address select 1,175,'HSRLayout',null,'KA','Bangalore',560065,'India',null,null
--Insert Into Address select 1,176,'Varthur',null,'KA','Bangalore',560012,'India',null,null
--Insert Into Address select 1,177,'Madiwala',null,'KA','Bangalore',560068,'India',null,null


--Insert Into Address select 1,178,'HSRLayout',null,'KA','Bangalore',560065,'India',null,null
--Insert Into Address select 1,179,'Varthur',null,'KA','Bangalore',560012,'India',null,null
--Insert Into Address select 1,180,'Madiwala',null,'KA','Bangalore',560068,'India',null,null


--Insert into InsuranceDetails select 'John','Snow',172,'9845122',19,'Self','1991-09-21'
--Insert into InsuranceDetails select 'Sara','Gilbert',173,'7525565',19,'Self','1988-10-10'
--Insert into InsuranceDetails select 'Kunal','Nayyar',174,'255365',19,'Self','1981-12-12'
--Insert into InsuranceDetails select 'Penny','Cuoco',175,'4534534',20,'Self','1990-09-21'
--Insert into InsuranceDetails select 'Aarti','Mann',176,'4535438',20,'Self','1977-10-10'
--Insert into InsuranceDetails select 'Bob','Newhart',177,'7897865',20,'Self','1981-12-12'
--Insert into InsuranceDetails select 'John','Ross',178,'453454354',21,'Self','1991-07-21'
--Insert into InsuranceDetails select 'Laura','Spencer',179,'78963245',21,'Self','1992-10-10'
--Insert into InsuranceDetails select 'Carol','Ann',180,'45345343',21,'Self','1981-12-12

--	select * from [dbo].[Appointment]

--select * from [dbo].[Doctor]

--select * from [dbo].[Customer]

--select * from [dbo].[Company]

--select * from [dbo].[Store]


--select * from InsuranceDetails