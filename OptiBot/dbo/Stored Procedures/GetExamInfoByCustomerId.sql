﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetExamInfoByCustomerId] 
	-- Add the parameters for the stored procedure here
	@CustomerId INT
AS
	BEGIN
		
if(@CustomerId > 0)
Begin

	select cust.ID as CustomerId, cust.FirstName, cust.LastName, c.ID as ExamId, p.Name as ExamType,
	c.ExamDate, c.ExpireDate, a.Id as HeaderId, a.Name as HeaderName,d.FirstName+' '+d.LastName as DoctorName,
	 ce.AttributeValue as SelectedListName, a.ListOrderType as HeaderType,c.Remarks as Remarks,
	a.DisplayOrderForList as HeaderDisplayOrder, cd.DocumentName as DocumentName,cd.DocumentPath as FilePath, 
	cd.DocumentContentType as DocumentContentType
	from 
	CustomerExamAttribute ce
	inner join CustomerExam c on c.ID = ce.CustomerExamId
	inner join Doctor d on d.ID = c.DoctorId
	inner join Customer cust on cust.ID = c.PatientId
	inner join [Attribute] a on a.Id = ce.AttributeId
	inner join Product p on p.Id = c.ProductId ---need to change into inner join once data is available
    left outer join CustomerDocument cd on cd.CustomerExamId = c.ID
	--inner join [AttributeValue] av on av.AttributeId = a.Id

	where cust.ID = @CustomerId
END

END
