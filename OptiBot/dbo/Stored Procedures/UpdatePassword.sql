﻿
CREATE PROCEDURE [dbo].[UpdatePassword] 
    @CompanyRefCode as varchar(20),
	@UserName 	as varchar(20),
	@Password 	as varchar(20)
AS
	BEGIN
		IF @UserName <> '' AND exists( SELECT * FROM Doctor d inner join Company c on c.ID=d.CompanyId WHERE UserName = @UserName AND c.CompanyRefCode= @CompanyRefCode)
		BEGIN
			Update Doctor SET [Password] = @Password FROM Doctor d inner join Company c on c.ID=d.CompanyId WHERE UserName = @UserName AND c.CompanyRefCode= @CompanyRefCode
			return 1
		END
		ELSE
		BEGIN
			return 0
		END	
	END

