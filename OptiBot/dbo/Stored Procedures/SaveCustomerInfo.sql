﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SaveCustomerInfo]
	-- Add the parameters for the stored procedure here
	
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@PhoneNumber VARCHAR(14),
	@DateOfBirth DATETIME,
	@EmailId VARCHAR(500),
	@Gender VARCHAR(1),
	@Age INT,
	@DoctorId INT,
	@Address VARCHAR(500),
	@City VARCHAR(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select * from Customer;

	
    -- Insert statements for procedure here
	INSERT INTO Customer(Uuid,FirstName,LastName,PhoneNumber,BotID,DateOfBirth,EmailID,Gender,FacebookID,Age,CompanyID) VALUES(null,@FirstName,@LastName,@PhoneNumber,null,@DateOfBirth,@EmailId,@Gender,null,@Age,(SELECT CompanyId FROM Doctor WHERE ID = @DoctorId))
	
	INSERT INTO Address(AddressType,ReferenceID,Address1,Address2,State,City,ZipCode,Country,Latitude,Longitude)  VALUES(1,SCOPE_IDENTITY(),@Address,null,null,@City,null,null,null,null)

END