﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DoctorLogin] 
	-- Add the parameters for the stored procedure here
	@EmployeeId INT, 
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50)
AS
	BEGIN
		SELECT d.EmployeeId FROM Doctor d
		WHERE d.EmployeeId = @EmployeeId AND d.UserName like @UserName AND d.Password = @Password
	END