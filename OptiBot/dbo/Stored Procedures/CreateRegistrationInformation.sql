﻿ -- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateRegistrationInformation]
@companyRefCode as varchar(20),
@companyName as varchar(50),
@storeAddress1 as varchar(100),
@storeAddress2 as varchar(100),
@storeCity as varchar(20),
@storePinCode as varchar(20),
@storeState as varchar(20),
@storeCountry as varchar(20),	
@DoctorFirstName as varchar(20)	,
@DoctorLastName as varchar(20)	,
@Gender as char(1),
@DOB as datetime,
@PhoneNumber as varchar(20),
@UserName 	as varchar(50),
@Password 	as varchar(20)
AS
	BEGIN

		Declare @companyId as int
		Declare @storeId as int
		Declare @doctorId as int
		Declare @addressType as int = 3
		Declare @addressId as int
		declare @attributeName as varchar(50)

		--If @companyRefCode = null @companyRefCode = ''
 
		If @companyRefCode <> '' AND exists( Select * from Company where CompanyRefCode = @companyRefCode)
			begin
				SET @companyId = (Select ID from Company where CompanyRefCode = @companyRefCode)
				SET @storeId = (Select top 1 ID from Store where CompanyID = @companyId)
				
				if Exists (Select * from Doctor where UserName = @UserName)
				begin
					return select '1' as CompanyRefCode  --UserName exits
				End
				If not exists(select * from Customer where CompanyID = @companyId and FirstName = @DoctorFirstName and LastName = @DoctorLastName)
			    Begin
					insert into Doctor select @DoctorFirstName, @DoctorLastName,@PhoneNumber,
					 'Ophthalmologist', @companyId,null,@UserName, @Password, null
					set @doctorId = @@identity
					update Doctor set EmployeeId = @doctorId where ID = @doctorId

					insert into StoreDoctor select @storeId, @doctorId
					insert into Address select 2,@doctorId, @storeAddress1, @storeAddress2,@storeState,@storeCity,@storePinCode, @storeCountry,null,null
                end

				--select employeeid from Doctor where ID = @doctorId
				select CompanyRefCode from Company where ID = @companyId-- = @companyRefCode 
			end
		
		else If @companyRefCode <> '' AND not exists( Select * from Company where CompanyRefCode = @companyRefCode)
			begin
			 return select '0' as CompanyRefCode --Company Ref Code not available
		    end

		else --Company is not available. Now system will create new company, store and doctor record
			  begin			   
				  insert into Company(CompanyName,Active) values( @companyName,1)
				--insert into company select 'ABC Optical',null, null,null,1,null,null
					set  @companyId = @@identity
					--select * from company
				
					update Company set CompanyRefCode =	substring(@companyName, 0,4) + REPLICATE('0',6-LEN(convert(varchar(10),@companyId))) + convert(varchar(10),@companyId)  where ID = @companyId
				
					insert into Address select 3,@companyId,@storeAddress1, @storeAddress2,@storeState,@storeCity,@storePinCode, @storeCountry,null,null
					set @addressId = @@identity
				
					insert into Store select @companyName,@companyId,@addressId,null,null,null,1,null,null,null,null,null,null,null
					set @storeId = @@identity
				
					insert into Doctor select @DoctorFirstName, @DoctorLastName,@PhoneNumber, 'Ophthalmologist', @companyId,null,@UserName, @Password, null
					set @doctorId = @@identity
				
					update Doctor set EmployeeId = @doctorId where ID = @doctorId
	
					insert into StoreDoctor select @storeId, @doctorId
					insert into Address select 2,@doctorId, @storeAddress1, @storeAddress2,@storeState,@storeCity,@storePinCode, @storeCountry,null,null

					exec dbo.[CreateNewCompanyDefaultAttributes] @companyId

					--select employeeid from Doctor where ID = @doctorId
					select CompanyRefCode from Company where ID = @companyId--CompanyRefCode = @companyRefCode 
              end			   
			  select CompanyRefCode from Company where ID = @companyId--CompanyRefCode = @companyRefCode 
	end