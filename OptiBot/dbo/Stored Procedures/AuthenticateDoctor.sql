﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AuthenticateDoctor] 
	-- Add the parameters for the stored procedure here
	@EmployeeId INT, 
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50)
AS
	BEGIN
		SELECT d.EmployeeId FROM Doctor d
		INNER JOIN Company c ON c.ID = d.CompanyId AND c.Active = 1
		WHERE d.EmployeeId = @EmployeeId AND d.UserName like @UserName AND d.Password = @Password
	END