﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.CreateAppointmentSchedule
	
	@CustomerId INT,
	@NextVisitedDoctorId INT,
	@DateOfAppointment DATETIME,
	@StartTime NVARCHAR(50),
	@EndTime NVARCHAR(50),
	@IsConfirmed BIT,
	@ReasonForVisitId INT,
	@AppointmentNotes NVARCHAR(500),
	@DoctorScheduleId INT
	
AS
	BEGIN
		INSERT INTO Appointment(CustomerID,DateOfService,LastVisitedDoctorID,NextVisitedDoctorID,DateOfAppointment,StartTime,EndTime,IsConfirmed,ReasonForVisitId,ReasonForVisit,DoctorScheduleID)
		VALUES (@CustomerId,null,null,@NextVisitedDoctorId,@DateOfAppointment,@StartTime,@EndTime,@IsConfirmed,@ReasonForVisitId,@AppointmentNotes,@DoctorScheduleId)
	END