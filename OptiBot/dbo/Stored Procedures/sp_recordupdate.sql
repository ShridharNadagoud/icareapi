﻿create procedure [dbo].[sp_recordupdate] 
@firstname varchar(50),
@doctorId int,
@Appointment_Date datetime,
@DateFrom datetime,
@DateTo datetime
As 
Begin

update [dbo].[Patient_Records]
set Appointment_Date=@Appointment_Date,time_slot_from=@DateFrom,time_slot_to=@DateTo,Next_Visit_DoctorId=@doctorId
where Patient_FirstName=@firstname

end