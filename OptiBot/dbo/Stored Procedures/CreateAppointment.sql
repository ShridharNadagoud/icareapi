﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.CreateAppointment
	
	@CustomerId INT,
	@NextVisitedDoctorId INT,
	@DateOfAppointment DATE,
	@StartTime NVARCHAR(50),
	@EndTime NVARCHAR(50),
	@IsConfirmed BIT,
	@ReasonForVisit INT,
	@AppointmentNotes NVARCHAR(500),
	@DoctorScheduleId INT
	
AS
	BEGIN
		INSERT INTO Appointment(CustomerID,DateOfService,LastVisitedDoctorID,NextVisitedDoctorID,DateOfAppointment,StartTime,EndTime,IsConfirmed,ReasonForVisitId,ReasonForVisit,DoctorScheduleID)
		VALUES (@CustomerId,null,null,@NextVisitedDoctorId,@DateOfAppointment,@StartTime,@EndTime,@IsConfirmed,@ReasonForVisit,@AppointmentNotes,@DoctorScheduleId)
	END