﻿CREATE PROCEDURE [dbo].[Sp_Notification] 
@Todate int NULL
AS
Begin
select cus.* ,app.DateOfAppointment,app.StartTime,doc.FirstName as Doctorfirstname,Doc.LastName as Doctorlastname
from Customer cus
INNER JOIN [dbo].[Appointment] app on app.CustomerID=cus.ID
Inner JOIN Doctor doc on doc.ID=app.NextVisitedDoctorID
WHERE app.DateOfAppointment>=getdate() and app.DateOfAppointment<=getdate()+30 and cus.FacebookID IS NOT NULL
End