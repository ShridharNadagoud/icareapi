﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SavePatientExam]
	-- Add the parameters for the stored procedure here
	@CustomerId INT,
	@DoctorId INT,
	@ExamDate DATE,
	@ExpireDate DATE,
	@Remarks VARCHAR(Max),
    @AttributeValueList VARCHAR(Max),
	@ProductId INT,
	@CustomerDocumentId as int
	--,
	--@Document VARBINARY(Max),
	--@DocumentName VARCHAR(50),
	--@DocumentContentType VARCHAR(50),
	--@DocumentPath VARCHAR(200) 
	

AS
BEGIN
    declare @customerExamId INT
	
	
	INSERT INTO CustomerExam([PatientId],[DoctorId],[ExamDate],[ExpireDate],[Remarks],[ProductId]) 
	VALUES (@CustomerId,@DoctorId,@ExamDate,@ExpireDate,@Remarks,@ProductId)
	set @customerExamId = @@identity
	--SCOPE_IDENTITY()
   
   INSERT INTO CustomerExamAttribute SELECT @customerExamId, AttributeId, AttributeValue
   FROM  dbo.GetAttributeData(@AttributeValueList) 
   IF(@CustomerDocumentId > 0)
   Begin
	Update CustomerDocument set CustomerExamId = @customerExamId, CustomerId = @CustomerId where ID = @CustomerDocumentId
   END
 --  INSERT INTO CustomerDocument([CustomerId],[CustomerExamId],[Document],[DocumentName],[DocumentContentType],[DocumentPath]) 
	--VALUES (@PatientId,@customerExamId,@Document,@DocumentName,@DocumentContentType,@DocumentPath)	
  
END   

