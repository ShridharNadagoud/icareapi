﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetExamInfo] 
	-- Add the parameters for the stored procedure here
	@ProductId INT
AS
	BEGIN
		
if(@ProductId > 0)

select p.Id as ProductId, p.Name as ProductName,
p.ProductTypeId as ProductTypeId, a.Id as HeaderId, a.Name as HeaderName,
av.Id as ListId, av.AttributeValue as ListName, a.ListOrderType as HeaderType,a.DisplayOrderForList as HeaderDisplayOrder  from Product p

inner join [ProductAttribute] pa on p.Id = pa.ProductId

inner join [Attribute] a on a.Id = pa.AttributeId

inner join [AttributeValue] av on av.AttributeId = a.Id

where p.Id = @ProductId
	
else

select Id as ProductId, Name as ProductName,
ProductTypeId from Product 

	END

