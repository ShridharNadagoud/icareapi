﻿CREATE PROCEDURE [dbo].[GetDoctorAppointmentSlot]
	-- Add the parameters for the stored procedure here
	@DoctorId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT s.StartTime as StartTime ,
(CASE WHEN s.StartTime < '12:00' then 'Morning' When s.StartTime >= '12:00'and s.StartTime < '16:00' then 'Afternoon' else 'Evening' end) as  [Session] FROM Doctor_Appointment_Slot s where s.DoctorId = @DoctorId order by s.StartTime
END