﻿
Create Function [dbo].[GetAttributeData] (@AttributeIdList Varchar(max) )  
Returns @Tbl_Attribute Table  ( AttributeId int, AttributeValue varchar(max))  
As  
Begin 
 Set @AttributeIdList =  @AttributeIdList + '|'
	DECLARE @AttributeIdpos INT 
	DECLARE @AttributeIdlen INT 
	DECLARE @AttributeValueId varchar(Max) 	
	set @AttributeIdpos = 0 
	set @AttributeIdlen = 0 	
   WHILE CHARINDEX('|', @AttributeIdList,@AttributeIdpos+1)	> 0 
	BEGIN 
		set @AttributeIdlen = CHARINDEX('|',@AttributeIdList,@AttributeIdpos+1) - @AttributeIdpos 
		set @AttributeValueId = SUBSTRING(@AttributeIdList,@AttributeIdpos, @AttributeIdlen) 
		
		insert into @Tbl_Attribute values
			(SUBSTRING(@AttributeValueId, 0, CHARINDEX(':',@AttributeValueId,0)),
			(SUBSTRING(@AttributeValueId, CHARINDEX(':',@AttributeValueId,0)+1,len(@AttributeValueId))) )

	set @AttributeIdpos = CHARINDEX('|',@AttributeIdList, @AttributeIdpos+@AttributeIdlen) + 1 
	END 
 
 Return
End
